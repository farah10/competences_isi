<?php

namespace App\Controller;

use App\Entity\MicroCompetence;
use App\Entity\SousCompetence;
use App\Entity\Ue;
use App\Form\AddUeType;
use App\Form\AdminMicroCompetenceType;
use App\Repository\CompetenceRepository;
use App\Repository\UeRepository;
use App\Service\UeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class AdminController extends AbstractController
{
    private $security;
    private $competenceRepository;
    private $ueRepository;

    public function __construct(Security $security, CompetenceRepository $competenceRepository, UeRepository $ueRepository)
    {
        $this->security = $security;
        $this->competenceRepository = $competenceRepository;
        $this->ueRepository = $ueRepository;
    }

    /**
     * @Route("/admin", name="app_admin")
     */
    public function index()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user = $this->security->getUser();

        return $this->render('admin.html.twig', [
            'user'=> $user,
        ]);
    }

    /**
     * @Route("/admin/ue", name="app_admin_ue")
     */
    public function adminUe(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $ues = $this->getDoctrine()
            ->getRepository(Ue::class)->findAll();
        return $this->render('ue/adminUe.html.twig', [
            'ues' => $ues,
        ]);
    }

    /**
     * @Route("/admin/microCompetence", name="app_admin_micro_competence")
     */
    public function adminMicroCompetence(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $form = $this->createForm(AdminMicroCompetenceType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sousCompetence = $form->getData()['sousCompetence'];
            $microcompetences = $sousCompetence->getMicroCompetences();
            return $this->render('micro_competence/adminMicroCompetence.html.twig', [
                'microcompetences' => $microcompetences,
                'form' => $form->createView(),
            ]);
        }

        return $this->render('micro_competence/adminMicroCompetence.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/sousCompetence", name="app_admin_sous_competence")
     */
    public function adminSousCompetence(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $sousCompetences = $this->getDoctrine()
                ->getRepository(SousCompetence::class)->findAll();
        return $this->render('sous_competence/adminSousCompetence.html.twig', [
            'sousCompetences' => $sousCompetences,
        ]);

    }


}
