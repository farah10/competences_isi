<?php

namespace App\Form;

use App\Entity\Competence;
use App\Entity\MicroCompetence;
use App\Entity\SousCompetence;
use App\Repository\CompetenceRepository;
use App\Repository\MicroCompetenceRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SousCompetenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextareaType::class, [
                'attr' => ['class' => 'form-control'],
            ])
            ->add('competence', EntityType::class, [
                'class' => Competence::class,
                'attr' => ['class' => 'form-control'],
                'choice_label' => 'name',
            ])
            ->add('microcompetences', EntityType::class, [
                'class' => MicroCompetence::class,
                'label' => false,
                'choice_label' => 'description',
                'attr' => ['class'=>'form-check'],
                'query_builder' => function (MicroCompetenceRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'ASC');
                },
                'multiple' => true,
                'expanded' => true,
                'choice_attr' => function($choice, $key, $value) {
                    $compName = "compIndefinie";
                    if($choice->getSousCompetences()->first()){
                        $compName = $choice->getSousCompetences()->first()->getCompetence()->getName();
                    }
                    return ['class'=>'form-check-input testcheck '.$compName];
                },
                'label_attr' => ['class'=>'form-check-label'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SousCompetence::class,
        ]);
    }
}
