<?php

namespace App\Form;

use App\Entity\Ue;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class AddUeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ue', EntityType::class, [
            'class' => Ue::class,
            'label' => false,
            'attr' => ['class' => 'form-control', 'required' => true],
            'choice_label' => function ($ue) {
                return $ue->getCode()." | ". $ue->getNom() ;
            }
            ])
            ->add('lettre', ChoiceType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'required' => true],
                'choices' => [
                    'Lettre obtenue' => [
                        'A' => 'A',
                        'B' => 'B',
                        'C' => 'C',
                        'D' => 'D',
                        'E' => 'E',
                    ]
                ],
            ]);
    }

}
