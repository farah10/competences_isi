<?php

namespace App\Controller;

use App\Entity\Competence;
use App\Entity\Ue;
use App\Service\UeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catalogue")
 */
class CatalogueController extends AbstractController
{
    private $ueService;

    public function __construct(UeService $ueService)
    {
        $this->ueService = $ueService;
    }

    /**
     * @Route("/competences", name="app_catalogue_competences")
     */
    public function competences()
    {
        $competences = $this->getDoctrine()
            ->getRepository(Competence::class)->findAll();
        return $this->render('catalogue/competences.html.twig', [
            'competences' => $competences,
        ]);
    }

    /**
     * @Route("/ues", name="app_catalogue_ues")
     */
    public function ues()
    {
        $infoUes = [];
        $ues = $this->getDoctrine()
            ->getRepository(Ue::class)->findAll();
        foreach($ues as $ue){
            array_push($infoUes,["ue"=>$ue, "competences"=>$this->ueService->getCompetencesParUe($ue)]);
        }
        return $this->render('catalogue/ues.html.twig', [
            'infoUes' => $infoUes,
        ]);
    }
}
