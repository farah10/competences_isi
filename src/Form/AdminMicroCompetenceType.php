<?php

namespace App\Form;

use App\Entity\Competence;
use App\Entity\MicroCompetence;
use App\Entity\SousCompetence;
use App\Repository\CompetenceRepository;
use App\Repository\SousCompetenceRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminMicroCompetenceType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('competence', EntityType::class, [
            'class' => Competence::class,
                'query_builder' => function (CompetenceRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'ASC');
                },
            'row_attr' => ['class' => 'form-group'],
            'attr' => ['class' => 'form-control', 'required' => true],
            'choice_label' => function ($competence) {
                return $competence->getName() ;
            }
            ])
            ->add('sousCompetence', EntityType::class, [
                        'class' => SousCompetence::class,
                        'query_builder' => function (SousCompetenceRepository $er) {
                            return $er->createQueryBuilder('u')
                                ->orderBy('u.id', 'ASC');
                        },
                        'row_attr' => ['class' => 'form-group'],
                        'attr' => ['class' => 'form-control', 'required' => true],
                        'choice_label' => function ($sousCompetence) {
                            return $sousCompetence->getDescription() ;
                        },
                        'choice_attr' => function($choice, $key, $value) {
                            return ['class' => $choice->getCompetence()->getName()];
                        },
            ]);


    }

}
