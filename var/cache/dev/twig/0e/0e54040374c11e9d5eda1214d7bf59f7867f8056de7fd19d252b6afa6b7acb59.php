<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* choixUe.html.twig */
class __TwigTemplate_d9bd9571127e378d2c2df8856ae6975d92e199682f93561c3caf4558503fd302 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "choixUe.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "choixUe.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Choisir mes UE";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head"));

        // line 6
        echo "    <script>
        // Configuration du chart
        const options = {
            colors: ['#F46036', '#6f2b72', '#1B998B', '#E71D36', '#6B7C24'],
            fontName: 'poppins',
            chartArea:{left:0,top:0,width:\"100%\",height:\"100%\"},
            pieHole: 0.4,
            legend: {position: 'center', textStyle: {fontSize: 15}}
        };

        function createChart(elementId, analyse, conception, realisation, deploiement, gestion ){
            google.charts.load(\"current\", {packages:[\"corechart\"]});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                    ['Competence', 'pourcentage par UE'],
                    ['Analyse', analyse],
                    ['Conception', conception],
                    ['Réalisation',  realisation],
                    ['Déploiement', deploiement],
                    ['Gestion',    gestion]
                ]);

                var chart = new google.visualization.PieChart(document.getElementById(elementId));
                chart.draw(data, options);
            }
        }
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 36
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 37
        echo "<div class=\"tab_container\">
    <h1>Choisir mes UE</h1>
    <hr class=\"my-4\">

    <div class=\"row\">
        <div class=\"au-card col-md-12\">
            <h6>Je souhaite développer mes compétences en ...</h6>
            ";
        // line 44
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 44, $this->source); })()), 'form_start');
        echo "
                ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 45, $this->source); })()), 'widget', ["attr" => ["class" => "choixue"]]);
        echo "
                <button class=\"btn btn-style-2\">";
        // line 46
        echo twig_escape_filter($this->env, (((isset($context["button_label"]) || array_key_exists("button_label", $context))) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 46, $this->source); })()), "Rechercher")) : ("Rechercher")), "html", null, true);
        echo "</button>
            ";
        // line 47
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 47, $this->source); })()), 'form_end');
        echo "
        </div>
    </div>

    ";
        // line 51
        if (((isset($context["infoUes"]) || array_key_exists("infoUes", $context)) &&  !twig_test_empty((isset($context["infoUes"]) || array_key_exists("infoUes", $context) ? $context["infoUes"] : (function () { throw new RuntimeError('Variable "infoUes" does not exist.', 51, $this->source); })())))) {
            // line 52
            echo "    <div class=\"row\">
        <div class=\"col-4\">
            <div class=\"list-group\" id=\"list-tab\" role=\"tablist\">
                ";
            // line 55
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["infoUes"]) || array_key_exists("infoUes", $context) ? $context["infoUes"] : (function () { throw new RuntimeError('Variable "infoUes" does not exist.', 55, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["infoUe"]) {
                // line 56
                echo "                    <a class=\"list-group-item list-group-item-action\"
                       data-toggle=\"list\"
                       role=\"tab\"
                       id=";
                // line 59
                echo twig_escape_filter($this->env, (("list-" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 59), "code", [], "any", false, false, false, 59)) . "-list"), "html", null, true);
                echo "
                       href=";
                // line 60
                echo twig_escape_filter($this->env, ("#list-" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 60), "code", [], "any", false, false, false, 60)), "html", null, true);
                echo "
                       aria-controls=";
                // line 61
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 61), "code", [], "any", false, false, false, 61), "html", null, true);
                echo ">
                        <strong>";
                // line 62
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 62), "code", [], "any", false, false, false, 62), "html", null, true);
                echo "</strong>
                        <br>
                        ";
                // line 64
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 64), "nom", [], "any", false, false, false, 64), "html", null, true);
                echo "
                    </a>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['infoUe'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 67
            echo "            </div>
        </div>
        <div class=\"col-8\">
            <div class=\"tab-content\" id=\"nav-tabContent\">
                ";
            // line 71
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["infoUes"]) || array_key_exists("infoUes", $context) ? $context["infoUes"] : (function () { throw new RuntimeError('Variable "infoUes" does not exist.', 71, $this->source); })()));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["infoUe"]) {
                // line 72
                echo "                    <div class=\"tab-pane fade\" role=\"tabpanel\" id=";
                echo twig_escape_filter($this->env, ("list-" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 72), "code", [], "any", false, false, false, 72)), "html", null, true);
                echo " aria-labelledby=";
                echo twig_escape_filter($this->env, (("list-" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 72), "code", [], "any", false, false, false, 72)) . "-list"), "html", null, true);
                echo ">
                        <div class=\"au-card\">
                            <span class=\"badge badge-pill badge-style\">";
                // line 74
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 74), "semestre", [], "any", false, false, false, 74), "html", null, true);
                echo "</span>
                            <span class=\"badge badge-pill badge-style2\">";
                // line 75
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 75), "type", [], "any", false, false, false, 75), "html", null, true);
                echo "</span>
                            <h5>
                                <strong>";
                // line 77
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 77), "code", [], "any", false, false, false, 77), "html", null, true);
                echo "</strong>
                                <span>";
                // line 78
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 78), "nom", [], "any", false, false, false, 78), "html", null, true);
                echo "</span>
                            </h5>
                            <hr>
                            <div class=\"row chartUe\" id=";
                // line 81
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 81), "code", [], "any", false, false, false, 81), "html", null, true);
                echo "></div>
                            <br>

                            <nav>
                                <div class=\"nav nav-tabs\" id=";
                // line 85
                echo twig_escape_filter($this->env, ("nav-tab" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 85), "code", [], "any", false, false, false, 85)), "html", null, true);
                echo " role=\"tablist\">
                                    <a class=\"nav-item nav-link active\" id=";
                // line 86
                echo twig_escape_filter($this->env, ("nav-objectif-tab" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 86), "code", [], "any", false, false, false, 86)), "html", null, true);
                echo " data-toggle=\"tab\" href=";
                echo twig_escape_filter($this->env, ("#nav-objectif" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 86), "code", [], "any", false, false, false, 86)), "html", null, true);
                echo " role=\"tab\" aria-controls=";
                echo twig_escape_filter($this->env, ("nav-objectif" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 86), "code", [], "any", false, false, false, 86)), "html", null, true);
                echo " aria-selected=\"true\">Objectif</a>
                                    <a class=\"nav-item nav-link\" id=";
                // line 87
                echo twig_escape_filter($this->env, ("nav-programme-tab" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 87), "code", [], "any", false, false, false, 87)), "html", null, true);
                echo " data-toggle=\"tab\" href=";
                echo twig_escape_filter($this->env, ("#nav-programme" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 87), "code", [], "any", false, false, false, 87)), "html", null, true);
                echo " role=\"tab\" aria-controls=";
                echo twig_escape_filter($this->env, ("nav-programme" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 87), "code", [], "any", false, false, false, 87)), "html", null, true);
                echo " aria-selected=\"false\">Programme</a>
                                    <a class=\"nav-item nav-link\" id=";
                // line 88
                echo twig_escape_filter($this->env, ("nav-competence-tab" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 88), "code", [], "any", false, false, false, 88)), "html", null, true);
                echo " data-toggle=\"tab\" href=";
                echo twig_escape_filter($this->env, ("#nav-competence" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 88), "code", [], "any", false, false, false, 88)), "html", null, true);
                echo " role=\"tab\" aria-controls=";
                echo twig_escape_filter($this->env, ("nav-competence" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 88), "code", [], "any", false, false, false, 88)), "html", null, true);
                echo " aria-selected=\"false\">Compétences</a>
                                </div>
                            </nav>

                            <div class=\"tab-content\" id=\"nav-tabContent\">
                                <div class=\"tab-pane fade show active\" id=";
                // line 93
                echo twig_escape_filter($this->env, ("nav-objectif" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 93), "code", [], "any", false, false, false, 93)), "html", null, true);
                echo " role=\"tabpanel\" aria-labelledby=";
                echo twig_escape_filter($this->env, ("nav-objectif-tab" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 93), "code", [], "any", false, false, false, 93)), "html", null, true);
                echo "><br>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 93), "description", [], "any", false, false, false, 93), "html", null, true);
                echo "</div>
                                <div class=\"tab-pane fade\" id=";
                // line 94
                echo twig_escape_filter($this->env, ("nav-programme" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 94), "code", [], "any", false, false, false, 94)), "html", null, true);
                echo " role=\"tabpanel\" aria-labelledby=";
                echo twig_escape_filter($this->env, ("nav-programme-tab" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 94), "code", [], "any", false, false, false, 94)), "html", null, true);
                echo "><br>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 94), "programme", [], "any", false, false, false, 94), "html", null, true);
                echo "</div>
                                <div class=\"tab-pane fade\" id=";
                // line 95
                echo twig_escape_filter($this->env, ("nav-competence" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 95), "code", [], "any", false, false, false, 95)), "html", null, true);
                echo " role=\"tabpanel\" aria-labelledby=";
                echo twig_escape_filter($this->env, ("nav-competence-tab" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 95), "code", [], "any", false, false, false, 95)), "html", null, true);
                echo "><br>
                                    <div class=\"table-responsive\">
                                        <table class=\"table table-hover tableStyle\">
                                            <tbody>
                                            ";
                // line 99
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 99), "microCompetences", [], "any", false, false, false, 99));
                foreach ($context['_seq'] as $context["_key"] => $context["microcompetence"]) {
                    // line 100
                    echo "                                                <tr>
                                                    <th scope=\"row\"><span class=\"badge badge-pill badge-style2\">";
                    // line 101
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["microcompetence"], "sousCompetences", [], "any", false, false, false, 101), "first", [], "any", false, false, false, 101), "competence", [], "any", false, false, false, 101), "name", [], "any", false, false, false, 101), "html", null, true);
                    echo "</span></th>
                                                    <td>";
                    // line 102
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["microcompetence"], "description", [], "any", false, false, false, 102), "html", null, true);
                    echo "</td>
                                                </tr>
                                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['microcompetence'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 105
                echo "                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                        createChart('";
                // line 113
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 113), "code", [], "any", false, false, false, 113), "html", null, true);
                echo "',
                                ";
                // line 114
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "competences", [], "any", false, false, false, 114), "analyse", [], "any", false, false, false, 114), "html", null, true);
                echo ",
                                ";
                // line 115
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "competences", [], "any", false, false, false, 115), "conception", [], "any", false, false, false, 115), "html", null, true);
                echo ",
                                ";
                // line 116
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "competences", [], "any", false, false, false, 116), "realisation", [], "any", false, false, false, 116), "html", null, true);
                echo ",
                                ";
                // line 117
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "competences", [], "any", false, false, false, 117), "deploiement", [], "any", false, false, false, 117), "html", null, true);
                echo ",
                                ";
                // line 118
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "competences", [], "any", false, false, false, 118), "gestion", [], "any", false, false, false, 118), "html", null, true);
                echo ")
                    </script>
                    ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 121
                echo "                    <p>Aucun résulat</p>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['infoUe'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 123
            echo "            </div>
        </div>
    </div>
    ";
        }
        // line 127
        echo "
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "choixUe.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  359 => 127,  353 => 123,  346 => 121,  338 => 118,  334 => 117,  330 => 116,  326 => 115,  322 => 114,  318 => 113,  308 => 105,  299 => 102,  295 => 101,  292 => 100,  288 => 99,  279 => 95,  271 => 94,  263 => 93,  251 => 88,  243 => 87,  235 => 86,  231 => 85,  224 => 81,  218 => 78,  214 => 77,  209 => 75,  205 => 74,  197 => 72,  192 => 71,  186 => 67,  177 => 64,  172 => 62,  168 => 61,  164 => 60,  160 => 59,  155 => 56,  151 => 55,  146 => 52,  144 => 51,  137 => 47,  133 => 46,  129 => 45,  125 => 44,  116 => 37,  109 => 36,  74 => 6,  67 => 5,  54 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Choisir mes UE{% endblock %}

{% block head %}
    <script>
        // Configuration du chart
        const options = {
            colors: ['#F46036', '#6f2b72', '#1B998B', '#E71D36', '#6B7C24'],
            fontName: 'poppins',
            chartArea:{left:0,top:0,width:\"100%\",height:\"100%\"},
            pieHole: 0.4,
            legend: {position: 'center', textStyle: {fontSize: 15}}
        };

        function createChart(elementId, analyse, conception, realisation, deploiement, gestion ){
            google.charts.load(\"current\", {packages:[\"corechart\"]});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                    ['Competence', 'pourcentage par UE'],
                    ['Analyse', analyse],
                    ['Conception', conception],
                    ['Réalisation',  realisation],
                    ['Déploiement', deploiement],
                    ['Gestion',    gestion]
                ]);

                var chart = new google.visualization.PieChart(document.getElementById(elementId));
                chart.draw(data, options);
            }
        }
    </script>
{% endblock %}

{% block body %}
<div class=\"tab_container\">
    <h1>Choisir mes UE</h1>
    <hr class=\"my-4\">

    <div class=\"row\">
        <div class=\"au-card col-md-12\">
            <h6>Je souhaite développer mes compétences en ...</h6>
            {{ form_start(form) }}
                {{ form_widget(form, { 'attr': {'class': 'choixue'} }) }}
                <button class=\"btn btn-style-2\">{{ button_label|default('Rechercher') }}</button>
            {{ form_end(form) }}
        </div>
    </div>

    {% if infoUes is defined and infoUes is not empty %}
    <div class=\"row\">
        <div class=\"col-4\">
            <div class=\"list-group\" id=\"list-tab\" role=\"tablist\">
                {% for infoUe in infoUes %}
                    <a class=\"list-group-item list-group-item-action\"
                       data-toggle=\"list\"
                       role=\"tab\"
                       id={{ 'list-' ~ infoUe.ue.code ~ '-list' }}
                       href={{ '#list-' ~ infoUe.ue.code }}
                       aria-controls={{ infoUe.ue.code }}>
                        <strong>{{ infoUe.ue.code }}</strong>
                        <br>
                        {{ infoUe.ue.nom }}
                    </a>
                {% endfor %}
            </div>
        </div>
        <div class=\"col-8\">
            <div class=\"tab-content\" id=\"nav-tabContent\">
                {% for infoUe in infoUes %}
                    <div class=\"tab-pane fade\" role=\"tabpanel\" id={{ 'list-' ~ infoUe.ue.code }} aria-labelledby={{ 'list-' ~ infoUe.ue.code ~ '-list' }}>
                        <div class=\"au-card\">
                            <span class=\"badge badge-pill badge-style\">{{ infoUe.ue.semestre }}</span>
                            <span class=\"badge badge-pill badge-style2\">{{ infoUe.ue.type }}</span>
                            <h5>
                                <strong>{{ infoUe.ue.code }}</strong>
                                <span>{{ infoUe.ue.nom }}</span>
                            </h5>
                            <hr>
                            <div class=\"row chartUe\" id={{ infoUe.ue.code }}></div>
                            <br>

                            <nav>
                                <div class=\"nav nav-tabs\" id={{ 'nav-tab' ~ infoUe.ue.code }} role=\"tablist\">
                                    <a class=\"nav-item nav-link active\" id={{ 'nav-objectif-tab' ~ infoUe.ue.code }} data-toggle=\"tab\" href={{ '#nav-objectif' ~ infoUe.ue.code }} role=\"tab\" aria-controls={{ 'nav-objectif' ~ infoUe.ue.code }} aria-selected=\"true\">Objectif</a>
                                    <a class=\"nav-item nav-link\" id={{ 'nav-programme-tab' ~ infoUe.ue.code }} data-toggle=\"tab\" href={{ '#nav-programme' ~ infoUe.ue.code }} role=\"tab\" aria-controls={{ 'nav-programme' ~ infoUe.ue.code }} aria-selected=\"false\">Programme</a>
                                    <a class=\"nav-item nav-link\" id={{ 'nav-competence-tab' ~ infoUe.ue.code }} data-toggle=\"tab\" href={{ '#nav-competence' ~ infoUe.ue.code }} role=\"tab\" aria-controls={{ 'nav-competence' ~ infoUe.ue.code }} aria-selected=\"false\">Compétences</a>
                                </div>
                            </nav>

                            <div class=\"tab-content\" id=\"nav-tabContent\">
                                <div class=\"tab-pane fade show active\" id={{ 'nav-objectif' ~ infoUe.ue.code }} role=\"tabpanel\" aria-labelledby={{ 'nav-objectif-tab' ~ infoUe.ue.code }}><br>{{ infoUe.ue.description }}</div>
                                <div class=\"tab-pane fade\" id={{ 'nav-programme' ~ infoUe.ue.code }} role=\"tabpanel\" aria-labelledby={{ 'nav-programme-tab' ~ infoUe.ue.code }}><br>{{ infoUe.ue.programme }}</div>
                                <div class=\"tab-pane fade\" id={{ 'nav-competence' ~ infoUe.ue.code }} role=\"tabpanel\" aria-labelledby={{ 'nav-competence-tab' ~ infoUe.ue.code }}><br>
                                    <div class=\"table-responsive\">
                                        <table class=\"table table-hover tableStyle\">
                                            <tbody>
                                            {% for microcompetence in infoUe.ue.microCompetences %}
                                                <tr>
                                                    <th scope=\"row\"><span class=\"badge badge-pill badge-style2\">{{microcompetence.sousCompetences.first.competence.name}}</span></th>
                                                    <td>{{microcompetence.description}}</td>
                                                </tr>
                                            {% endfor %}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                        createChart('{{ infoUe.ue.code }}',
                                {{ infoUe.competences.analyse }},
                                {{ infoUe.competences.conception }},
                                {{ infoUe.competences.realisation }},
                                {{ infoUe.competences.deploiement }},
                                {{ infoUe.competences.gestion }})
                    </script>
                    {% else %}
                    <p>Aucun résulat</p>
                {% endfor %}
            </div>
        </div>
    </div>
    {% endif %}

</div>
{% endblock %}
", "choixUe.html.twig", "/var/www/tx_back/competences_isi/templates/choixUe.html.twig");
    }
}
