<?php

namespace App\Service;

use App\Entity\Competence;
use App\Entity\MicroCompetence;
use App\Entity\Ue;
use Doctrine\Common\Collections\ArrayCollection;
use PhpParser\Node\Stmt\Foreach_;


class UeService
{
    public function getCompetencesParUe($ue){
        $competencesParUe = ['analyse'=>0,'conception'=>0,'realisation'=>0,'deploiement'=>0,'gestion'=>0];
        $microcompetences = $ue->getMicroCompetences();

        foreach($microcompetences as $microcompetence){
            if($microcompetence->getSousCompetences()->first()){
                $competencesParUe[$microcompetence->getSousCompetences()->first()->getCompetence()->getName()] ++;
            }
        }

        // Rendre en pourcent les compétences par UE
        $totalMicroCompetences = $microcompetences->count();
        foreach($competencesParUe as $competence=>$value){
            if($value != 0){
                $competencesParUe[$competence] = round($value*100/$totalMicroCompetences);
            }
        }
        return $competencesParUe;

    }

    public function getProfilCompetence($ues){
        $competencesParProfil = ['analyse'=>0,'conception'=>0,'realisation'=>0,'deploiement'=>0,'gestion'=>0];
        foreach($ues as $ue){
            $microcompetences = $ue->getMicroCompetences();
            foreach($microcompetences as $microcompetence){
                $competencesParProfil[$microcompetence->getSousCompetences()->first()->getCompetence()->getName()] ++;
            }
        }
        return $competencesParProfil;

    }

    public function searchUesFromCompetences($competences) {
        $infoUes = [];
        foreach($competences as $competence){
            $sousComps = $competence->getSousCompetences();
            foreach($sousComps as $sousComp){
                $microComps = $sousComp->getMicroCompetences();
                foreach($microComps as $microComp){
                    foreach($microComp->getUes() as $ue){
                        $competencesParUe = $this->getCompetencesParUe($ue);
                        if($competencesParUe[$competence->getName()] >= 40){
                            $infoUe = ["ue"=>$ue, "competences"=>$competencesParUe];
                            if (!in_array ( $infoUe, $infoUes)) {
                                array_push($infoUes,$infoUe);
                            }
                        }
                    }
                }
            }
            $infoUes = $this->orderArrayByCompetence($infoUes, $competence->getName());
        }
        return $infoUes;
    }

    // Classe les UE par ordre décroissant du pourcentage de compétence souhaiter
    private function orderArrayByCompetence($array, $competence){
        $count = count($array);
        $placeholder = array();
        for ( $i1=0; $i1<$count; $i1++ ) {
            for ( $i2=0; $i2<$count; $i2++ ) {
                if ( $i1 == $i2 ) continue;
                if ( $array[$i1]['competences'][$competence] > $array[$i2]['competences'][$competence] ) {
                    $placeholder = $array[$i1];
                    $array[$i1] = $array[$i2];
                    $array[$i2] = $placeholder;
                }
            }
        }
        return $array;
    }

    public function getMicroCompetenceFromProfil($competences, $ues){
        $microCompetences = [];
        foreach($competences as $competence){
            $resultat = new ArrayCollection();
            foreach ($ues as $ue){
                foreach ($ue->getMicroCompetences() as $microCompetence){
                    if(!$resultat->contains($microCompetence) &&
                    $competence->getName() == $microCompetence->getSousCompetences()->first()->getCompetence()->getName()
                    ){
                        $resultat->add($microCompetence);
                    }
                }
            }

            // Retrourner la liste des microcompétences triées par ordre alphabétique
            $iterator = $resultat->getIterator();
            $iterator->uasort(function (MicroCompetence $a, MicroCompetence $b) {
                return $a->getDescription() <=> $b->getDescription();
            });

            $microCompetences[$competence->getName()] = new ArrayCollection(iterator_to_array($iterator));
        }
        return $microCompetences;
    }

}
