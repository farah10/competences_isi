#  Profil de compétences ISI

## Intro

Cette application est destinée aux étudiants ISI pour se construire un parcours personalisé et consulter leur profil de compétences.

## Installation

1. Cette application tourne sur Symfony 5.1. et sur un serveur apache 2.4
    - Elle nécessite le gestionnaire de dépendance "Composer".
    



2. Une fois le dépôt en cloner en local veuillez installer les dépendance suivantes
    - ``composer require annotations``
    - ``composer require asset``
    - ``composer require symfony/form``
    - ``composer require doctrine``
    - ``composer require dompdf``
    - ``symfony/validator``




3. Modifier la ligne suivante du fichier .env pour travailler sur votre base de données locales, 
    - ``DATABASE_URL="mysql://identifiant_local_database:pwd_local_database@127.0.0.1:3306/my_database_name"``
    



4. Pour récupérer les données de la base en production, vous pouvez accédez au client phpmyadmin et exporter la base pour l'importer en local.
    - Utilisateur phpmyadmin : root 
    - Mdp phpmydmin : U6bun2 

