<?php

namespace App\Controller;

use App\Entity\Competence;
use App\Entity\Ue;
use App\Service\UeService;
use App\Form\ChoixUeType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ChoixUeController extends AbstractController
{
    private $ueService;

    public function __construct(UeService $ueService)
    {
        $this->ueService = $ueService;
    }

    /**
     * @Route("/choixUE", name="app_choixUe")
     */
    public function index(Request $request): Response
    {
        $form = $this->createForm(ChoixUeType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $infoUes = $this->ueService->searchUesFromCompetences($form->getData());
            return $this->render('choixUe.html.twig', [
                'infoUes' => $infoUes,
                'form' => $form->createView(),
            ]);
        }

        return $this->render('choixUe.html.twig', [
            'form' => $form->createView(),
        ]);

    }

}
