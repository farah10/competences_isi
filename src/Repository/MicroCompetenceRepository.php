<?php

namespace App\Repository;

use App\Entity\MicroCompetence;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MicroCompetence|null find($id, $lockMode = null, $lockVersion = null)
 * @method MicroCompetence|null findOneBy(array $criteria, array $orderBy = null)
 * @method MicroCompetence[]    findAll()
 * @method MicroCompetence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MicroCompetenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MicroCompetence::class);
    }

    // /**
    //  * @return MicroCompetence[] Returns an array of MicroCompetence objects
    //  */

    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MicroCompetence
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
