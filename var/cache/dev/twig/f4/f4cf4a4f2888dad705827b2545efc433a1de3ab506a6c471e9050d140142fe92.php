<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* catalogue/competences.html.twig */
class __TwigTemplate_f5b052de0b96c85f76a1ce62746c03301f5060ca27dffc2c9efb4dee6bcd0d4b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "catalogue/competences.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "catalogue/competences.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Catalogue des compétences";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"tab_container\">

        <h1>Catalogue des compétences</h1>
        <hr class=\"my-4\">

        <input class=\"inputTab\" id=\"tab1\" type=\"radio\" name=\"tabs\" checked>
        <label class=\"competence\" for=\"tab1\"><span>Analyse</span></label>

        <input class=\"inputTab\" id=\"tab2\" type=\"radio\" name=\"tabs\">
        <label class=\"competence\" for=\"tab2\"><span>Conception</span></label>

        <input class=\"inputTab\" id=\"tab3\" type=\"radio\" name=\"tabs\">
        <label class=\"competence\" for=\"tab3\"><span>Réalisation</span></label>

        <input class=\"inputTab\" id=\"tab4\" type=\"radio\" name=\"tabs\">
        <label class=\"competence\" for=\"tab4\"><span>Déploiement</span></label>

        <input class=\"inputTab\" id=\"tab5\" type=\"radio\" name=\"tabs\">
        <label class=\"competence\"  for=\"tab5\"><span>Gestion</span></label>

        ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["competences"]) || array_key_exists("competences", $context) ? $context["competences"] : (function () { throw new RuntimeError('Variable "competences" does not exist.', 26, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["competence"]) {
            // line 27
            echo "            <section id=";
            echo twig_escape_filter($this->env, ("content" . twig_get_attribute($this->env, $this->source, $context["competence"], "id", [], "any", false, false, false, 27)), "html", null, true);
            echo " class=\"tab-content\">
                <div class=\"row\">
                    <div class=\"au-card col-md-12\">
                        <h5>";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["competence"], "completeName", [], "any", false, false, false, 30), "html", null, true);
            echo "</h5>
                        <div class=\"line\"></div>
                        <div class=\"panel-group accordion\" role=\"tablist\"
                             aria-multiselectable=\"true\" id=";
            // line 33
            echo twig_escape_filter($this->env, ("accordion" . twig_get_attribute($this->env, $this->source, $context["competence"], "id", [], "any", false, false, false, 33)), "html", null, true);
            echo ">
                            ";
            // line 34
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["competence"], "sousCompetences", [], "any", false, false, false, 34));
            foreach ($context['_seq'] as $context["_key"] => $context["sousCompetence"]) {
                // line 35
                echo "                                <div class=\"panel panel-default\">
                                    <div class=\"panel-heading\" role=\"tab\" id=";
                // line 36
                echo twig_escape_filter($this->env, ("heading" . twig_get_attribute($this->env, $this->source, $context["sousCompetence"], "id", [], "any", false, false, false, 36)), "html", null, true);
                echo ">
                                        <h4 class=\"panel-title\">
                                            <a role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\"
                                               aria-expanded=\"false\"  href=";
                // line 39
                echo twig_escape_filter($this->env, ("#collapse" . twig_get_attribute($this->env, $this->source, $context["sousCompetence"], "id", [], "any", false, false, false, 39)), "html", null, true);
                echo "
                                               aria-controls=";
                // line 40
                echo twig_escape_filter($this->env, ("collapse" . twig_get_attribute($this->env, $this->source, $context["sousCompetence"], "id", [], "any", false, false, false, 40)), "html", null, true);
                echo ">
                                                ";
                // line 41
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["sousCompetence"], "description", [], "any", false, false, false, 41), "html", null, true);
                echo "
                                            </a>
                                        </h4>
                                    </div>
                                    <div id=\"";
                // line 45
                echo twig_escape_filter($this->env, ("collapse" . twig_get_attribute($this->env, $this->source, $context["sousCompetence"], "id", [], "any", false, false, false, 45)), "html", null, true);
                echo "\" class=\"panel-collapse collapse in\"
                                         role=\"tabpanel\" aria-labelledby=";
                // line 46
                echo twig_escape_filter($this->env, ("heading" . twig_get_attribute($this->env, $this->source, $context["sousCompetence"], "id", [], "any", false, false, false, 46)), "html", null, true);
                echo ">
                                        <div class=\"panel-body\">
                                        ";
                // line 48
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["sousCompetence"], "microCompetences", [], "any", false, false, false, 48));
                foreach ($context['_seq'] as $context["_key"] => $context["microCompetence"]) {
                    // line 49
                    echo "                                            <table class=\"table borderless table-sm\">
                                                    <tr>
                                                        <td>";
                    // line 51
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["microCompetence"], "description", [], "any", false, false, false, 51), "html", null, true);
                    echo "</td>
                                                        <td>
                                                        ";
                    // line 53
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["microCompetence"], "ues", [], "any", false, false, false, 53));
                    foreach ($context['_seq'] as $context["_key"] => $context["ue"]) {
                        // line 54
                        echo "                                                            <span class=\"badge badge-style\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ue"], "code", [], "any", false, false, false, 54), "html", null, true);
                        echo "</span>
                                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ue'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 56
                    echo "                                                        </td>
                                                    </tr>
                                            </table>
                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['microCompetence'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 60
                echo "                                        </div>
                                    </div>
                                </div>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sousCompetence'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo "                        </div>
                    </div>
                </div>
            </section>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 69
            echo "            <p>Aucun résulat</p>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['competence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "catalogue/competences.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 71,  207 => 69,  198 => 64,  189 => 60,  180 => 56,  171 => 54,  167 => 53,  162 => 51,  158 => 49,  154 => 48,  149 => 46,  145 => 45,  138 => 41,  134 => 40,  130 => 39,  124 => 36,  121 => 35,  117 => 34,  113 => 33,  107 => 30,  100 => 27,  95 => 26,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Catalogue des compétences{% endblock %}

{% block body %}
    <div class=\"tab_container\">

        <h1>Catalogue des compétences</h1>
        <hr class=\"my-4\">

        <input class=\"inputTab\" id=\"tab1\" type=\"radio\" name=\"tabs\" checked>
        <label class=\"competence\" for=\"tab1\"><span>Analyse</span></label>

        <input class=\"inputTab\" id=\"tab2\" type=\"radio\" name=\"tabs\">
        <label class=\"competence\" for=\"tab2\"><span>Conception</span></label>

        <input class=\"inputTab\" id=\"tab3\" type=\"radio\" name=\"tabs\">
        <label class=\"competence\" for=\"tab3\"><span>Réalisation</span></label>

        <input class=\"inputTab\" id=\"tab4\" type=\"radio\" name=\"tabs\">
        <label class=\"competence\" for=\"tab4\"><span>Déploiement</span></label>

        <input class=\"inputTab\" id=\"tab5\" type=\"radio\" name=\"tabs\">
        <label class=\"competence\"  for=\"tab5\"><span>Gestion</span></label>

        {% for competence in competences %}
            <section id={{ 'content' ~ competence.id }} class=\"tab-content\">
                <div class=\"row\">
                    <div class=\"au-card col-md-12\">
                        <h5>{{ competence.completeName }}</h5>
                        <div class=\"line\"></div>
                        <div class=\"panel-group accordion\" role=\"tablist\"
                             aria-multiselectable=\"true\" id={{ 'accordion' ~ competence.id }}>
                            {% for sousCompetence in competence.sousCompetences %}
                                <div class=\"panel panel-default\">
                                    <div class=\"panel-heading\" role=\"tab\" id={{ 'heading' ~ sousCompetence.id }}>
                                        <h4 class=\"panel-title\">
                                            <a role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\"
                                               aria-expanded=\"false\"  href={{ '#collapse' ~ sousCompetence.id }}
                                               aria-controls={{ 'collapse' ~ sousCompetence.id }}>
                                                {{sousCompetence.description}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id=\"{{ 'collapse' ~ sousCompetence.id }}\" class=\"panel-collapse collapse in\"
                                         role=\"tabpanel\" aria-labelledby={{ 'heading' ~ sousCompetence.id }}>
                                        <div class=\"panel-body\">
                                        {% for microCompetence in sousCompetence.microCompetences %}
                                            <table class=\"table borderless table-sm\">
                                                    <tr>
                                                        <td>{{microCompetence.description}}</td>
                                                        <td>
                                                        {% for ue in microCompetence.ues %}
                                                            <span class=\"badge badge-style\">{{ue.code}}</span>
                                                        {% endfor %}
                                                        </td>
                                                    </tr>
                                            </table>
                                        {% endfor %}
                                        </div>
                                    </div>
                                </div>
                            {% endfor %}
                        </div>
                    </div>
                </div>
            </section>
        {% else %}
            <p>Aucun résulat</p>
        {% endfor %}

    </div>
{% endblock %}
", "catalogue/competences.html.twig", "/var/www/tx_back/competences_isi/templates/catalogue/competences.html.twig");
    }
}
