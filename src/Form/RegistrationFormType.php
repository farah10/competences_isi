<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder'=>'Email UTT', 'required' => true, 'autofocus'=>true],
            ])
            ->add('firstname', TextType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder'=>'Prénom', 'required' => true, 'autofocus'=>true],
            ])
            ->add('lastname', TextType::class, [
                'label' => false,
                'attr' => ['class' => 'form-control', 'placeholder'=>'Nom', 'required' => true, 'autofocus'=>true],
            ])
            ->add('plainPassword', RepeatedType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'type' => PasswordType::class,
                'mapped' => false,
                'first_options'  => array('label' => false, 'attr' => ['class' => 'form-control', 'placeholder'=>'Mot de passe', 'required' => true]),
                'second_options' => array('label' => false, 'attr' => ['class' => 'form-control', 'placeholder'=>'Confirmer le mot de passe', 'required' => true]),
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez rentrer un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit contenir un minimum de  {{ limit }} caractères',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
