<?php

namespace App\Form;

use App\Entity\Competence;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
//use Symfony\Component\OptionsResolver\OptionsResolver;

class ChoixUeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('competence', EntityType::class, [
            'class' => Competence::class,
            'label' => false,
            'attr' => ['class' => 'form-control', 'required' => true],
            'choice_label' => function ($competence) {
                return $competence->getName();
            }
        ]);
    }

}
