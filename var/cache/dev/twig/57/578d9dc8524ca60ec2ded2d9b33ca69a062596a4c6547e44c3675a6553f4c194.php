<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* catalogue/ues.html.twig */
class __TwigTemplate_ddce299c4d13a61bc8aef3d0252f6cddf9fe1af3379edf5183a2e10f58990cf1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "catalogue/ues.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "catalogue/ues.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Catalogue des UE";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head"));

        // line 6
        echo "    <script>
        // Configuration du chart
        const options = {
            colors: ['#F46036', '#6f2b72', '#1B998B', '#E71D36', '#6B7C24'],
            fontName: 'poppins',
            chartArea:{left:0,top:0,width:\"100%\",height:\"100%\"},
            pieHole: 0.4,
            legend: {position: 'center', textStyle: {fontSize: 15}}
        };

        function createChart(elementId, analyse, conception, realisation, deploiement, gestion ){
            google.charts.load(\"current\", {packages:[\"corechart\"]});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                    ['Competence', 'pourcentage par UE'],
                    ['Analyse', analyse],
                    ['Conception', conception],
                    ['Réalisation',  realisation],
                    ['Déploiement', deploiement],
                    ['Gestion',    gestion]
                ]);

                var chart = new google.visualization.PieChart(document.getElementById(elementId));
                chart.draw(data, options);
            }
        }
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 36
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 37
        echo "<div class=\"tab_container\">

    <h1>Catalogue des UEs</h1>
    <hr class=\"my-4\">

    <div class=\"row\">
        <div class=\"col-4\">
            <div class=\"list-group\" id=\"list-tab\" role=\"tablist\">
                ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["infoUes"]) || array_key_exists("infoUes", $context) ? $context["infoUes"] : (function () { throw new RuntimeError('Variable "infoUes" does not exist.', 45, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["infoUe"]) {
            // line 46
            echo "                    <a class=\"list-group-item list-group-item-action\"
                       data-toggle=\"list\"
                       role=\"tab\"
                       id=";
            // line 49
            echo twig_escape_filter($this->env, (("list-" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 49), "code", [], "any", false, false, false, 49)) . "-list"), "html", null, true);
            echo "
                       href=";
            // line 50
            echo twig_escape_filter($this->env, ("#list-" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 50), "code", [], "any", false, false, false, 50)), "html", null, true);
            echo "
                       aria-controls=";
            // line 51
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 51), "code", [], "any", false, false, false, 51), "html", null, true);
            echo ">
                        <strong>";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 52), "code", [], "any", false, false, false, 52), "html", null, true);
            echo "</strong>
                        <br>
                        ";
            // line 54
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 54), "nom", [], "any", false, false, false, 54), "html", null, true);
            echo "
                    </a>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['infoUe'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "            </div>
        </div>
        <div class=\"col-8\">
            <div class=\"tab-content\" id=\"nav-tabContent\">
                ";
        // line 61
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["infoUes"]) || array_key_exists("infoUes", $context) ? $context["infoUes"] : (function () { throw new RuntimeError('Variable "infoUes" does not exist.', 61, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["infoUe"]) {
            // line 62
            echo "                    <div class=\"tab-pane fade\" role=\"tabpanel\" id=";
            echo twig_escape_filter($this->env, ("list-" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 62), "code", [], "any", false, false, false, 62)), "html", null, true);
            echo " aria-labelledby=";
            echo twig_escape_filter($this->env, (("list-" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 62), "code", [], "any", false, false, false, 62)) . "-list"), "html", null, true);
            echo ">
                        <div class=\"au-card\">
                            <span class=\"badge badge-pill badge-style2\">";
            // line 64
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 64), "semestre", [], "any", false, false, false, 64), "html", null, true);
            echo "</span>
                            <span class=\"badge badge-pill badge-style\">";
            // line 65
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 65), "type", [], "any", false, false, false, 65), "html", null, true);
            echo "</span>
                            <h5>
                                <strong>";
            // line 67
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 67), "code", [], "any", false, false, false, 67), "html", null, true);
            echo "</strong>
                                <span>";
            // line 68
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 68), "nom", [], "any", false, false, false, 68), "html", null, true);
            echo "</span>
                            </h5>
                            <hr>
                            <div class=\"row chartUe\" id=";
            // line 71
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 71), "code", [], "any", false, false, false, 71), "html", null, true);
            echo "></div>
                            <br>

                            <nav>
                                <div class=\"nav nav-tabs\" id=";
            // line 75
            echo twig_escape_filter($this->env, ("nav-tab" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 75), "code", [], "any", false, false, false, 75)), "html", null, true);
            echo " role=\"tablist\">
                                    <a class=\"nav-item nav-link active\" id=";
            // line 76
            echo twig_escape_filter($this->env, ("nav-objectif-tab" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 76), "code", [], "any", false, false, false, 76)), "html", null, true);
            echo " data-toggle=\"tab\" href=";
            echo twig_escape_filter($this->env, ("#nav-objectif" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 76), "code", [], "any", false, false, false, 76)), "html", null, true);
            echo " role=\"tab\" aria-controls=";
            echo twig_escape_filter($this->env, ("nav-objectif" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 76), "code", [], "any", false, false, false, 76)), "html", null, true);
            echo " aria-selected=\"true\">Objectif</a>
                                    <a class=\"nav-item nav-link\" id=";
            // line 77
            echo twig_escape_filter($this->env, ("nav-programme-tab" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 77), "code", [], "any", false, false, false, 77)), "html", null, true);
            echo " data-toggle=\"tab\" href=";
            echo twig_escape_filter($this->env, ("#nav-programme" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 77), "code", [], "any", false, false, false, 77)), "html", null, true);
            echo " role=\"tab\" aria-controls=";
            echo twig_escape_filter($this->env, ("nav-programme" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 77), "code", [], "any", false, false, false, 77)), "html", null, true);
            echo " aria-selected=\"false\">Programme</a>
                                    <a class=\"nav-item nav-link\" id=";
            // line 78
            echo twig_escape_filter($this->env, ("nav-competence-tab" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 78), "code", [], "any", false, false, false, 78)), "html", null, true);
            echo " data-toggle=\"tab\" href=";
            echo twig_escape_filter($this->env, ("#nav-competence" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 78), "code", [], "any", false, false, false, 78)), "html", null, true);
            echo " role=\"tab\" aria-controls=";
            echo twig_escape_filter($this->env, ("nav-competence" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 78), "code", [], "any", false, false, false, 78)), "html", null, true);
            echo " aria-selected=\"false\">Compétences</a>
                                </div>
                            </nav>

                            <div class=\"tab-content\" id=\"nav-tabContent\">
                                <div class=\"tab-pane fade show active\" id=";
            // line 83
            echo twig_escape_filter($this->env, ("nav-objectif" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 83), "code", [], "any", false, false, false, 83)), "html", null, true);
            echo " role=\"tabpanel\" aria-labelledby=";
            echo twig_escape_filter($this->env, ("nav-objectif-tab" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 83), "code", [], "any", false, false, false, 83)), "html", null, true);
            echo "><br>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 83), "description", [], "any", false, false, false, 83), "html", null, true);
            echo "</div>
                                <div class=\"tab-pane fade\" id=";
            // line 84
            echo twig_escape_filter($this->env, ("nav-programme" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 84), "code", [], "any", false, false, false, 84)), "html", null, true);
            echo " role=\"tabpanel\" aria-labelledby=";
            echo twig_escape_filter($this->env, ("nav-programme-tab" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 84), "code", [], "any", false, false, false, 84)), "html", null, true);
            echo "><br>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 84), "programme", [], "any", false, false, false, 84), "html", null, true);
            echo "</div>
                                <div class=\"tab-pane fade\" id=";
            // line 85
            echo twig_escape_filter($this->env, ("nav-competence" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 85), "code", [], "any", false, false, false, 85)), "html", null, true);
            echo " role=\"tabpanel\" aria-labelledby=";
            echo twig_escape_filter($this->env, ("nav-competence-tab" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 85), "code", [], "any", false, false, false, 85)), "html", null, true);
            echo "><br>
                                    <div class=\"table-responsive\">
                                        <table class=\"table table-hover tableStyle\">
                                            <tbody>
                                            ";
            // line 89
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 89), "microCompetences", [], "any", false, false, false, 89));
            foreach ($context['_seq'] as $context["_key"] => $context["microcompetence"]) {
                // line 90
                echo "                                                ";
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["microcompetence"], "sousCompetences", [], "any", false, false, false, 90), "first", [], "any", false, false, false, 90)) {
                    // line 91
                    echo "                                                    <tr>
                                                        <th scope=\"row\"><span class=\"badge badge-pill badge-style2\">";
                    // line 92
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["microcompetence"], "sousCompetences", [], "any", false, false, false, 92), "first", [], "any", false, false, false, 92), "competence", [], "any", false, false, false, 92), "name", [], "any", false, false, false, 92), "html", null, true);
                    echo "</span></th>
                                                        <td>";
                    // line 93
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["microcompetence"], "description", [], "any", false, false, false, 93), "html", null, true);
                    echo "</td>
                                                    </tr>
                                                ";
                }
                // line 96
                echo "                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['microcompetence'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 97
            echo "                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                        createChart('";
            // line 105
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "ue", [], "any", false, false, false, 105), "code", [], "any", false, false, false, 105), "html", null, true);
            echo "',
                            ";
            // line 106
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "competences", [], "any", false, false, false, 106), "analyse", [], "any", false, false, false, 106), "html", null, true);
            echo ",
                            ";
            // line 107
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "competences", [], "any", false, false, false, 107), "conception", [], "any", false, false, false, 107), "html", null, true);
            echo ",
                            ";
            // line 108
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "competences", [], "any", false, false, false, 108), "realisation", [], "any", false, false, false, 108), "html", null, true);
            echo ",
                            ";
            // line 109
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "competences", [], "any", false, false, false, 109), "deploiement", [], "any", false, false, false, 109), "html", null, true);
            echo ",
                            ";
            // line 110
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["infoUe"], "competences", [], "any", false, false, false, 110), "gestion", [], "any", false, false, false, 110), "html", null, true);
            echo ")
                    </script>
                ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 113
            echo "                    <p>Aucun résulat</p>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['infoUe'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 115
        echo "            </div>
        </div>
    </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "catalogue/ues.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  334 => 115,  327 => 113,  319 => 110,  315 => 109,  311 => 108,  307 => 107,  303 => 106,  299 => 105,  289 => 97,  283 => 96,  277 => 93,  273 => 92,  270 => 91,  267 => 90,  263 => 89,  254 => 85,  246 => 84,  238 => 83,  226 => 78,  218 => 77,  210 => 76,  206 => 75,  199 => 71,  193 => 68,  189 => 67,  184 => 65,  180 => 64,  172 => 62,  167 => 61,  161 => 57,  152 => 54,  147 => 52,  143 => 51,  139 => 50,  135 => 49,  130 => 46,  126 => 45,  116 => 37,  109 => 36,  74 => 6,  67 => 5,  54 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Catalogue des UE{% endblock %}

{% block head %}
    <script>
        // Configuration du chart
        const options = {
            colors: ['#F46036', '#6f2b72', '#1B998B', '#E71D36', '#6B7C24'],
            fontName: 'poppins',
            chartArea:{left:0,top:0,width:\"100%\",height:\"100%\"},
            pieHole: 0.4,
            legend: {position: 'center', textStyle: {fontSize: 15}}
        };

        function createChart(elementId, analyse, conception, realisation, deploiement, gestion ){
            google.charts.load(\"current\", {packages:[\"corechart\"]});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                    ['Competence', 'pourcentage par UE'],
                    ['Analyse', analyse],
                    ['Conception', conception],
                    ['Réalisation',  realisation],
                    ['Déploiement', deploiement],
                    ['Gestion',    gestion]
                ]);

                var chart = new google.visualization.PieChart(document.getElementById(elementId));
                chart.draw(data, options);
            }
        }
    </script>
{% endblock %}

{% block body %}
<div class=\"tab_container\">

    <h1>Catalogue des UEs</h1>
    <hr class=\"my-4\">

    <div class=\"row\">
        <div class=\"col-4\">
            <div class=\"list-group\" id=\"list-tab\" role=\"tablist\">
                {% for infoUe in infoUes %}
                    <a class=\"list-group-item list-group-item-action\"
                       data-toggle=\"list\"
                       role=\"tab\"
                       id={{ 'list-' ~ infoUe.ue.code ~ '-list' }}
                       href={{ '#list-' ~ infoUe.ue.code }}
                       aria-controls={{ infoUe.ue.code }}>
                        <strong>{{ infoUe.ue.code }}</strong>
                        <br>
                        {{ infoUe.ue.nom }}
                    </a>
                {% endfor %}
            </div>
        </div>
        <div class=\"col-8\">
            <div class=\"tab-content\" id=\"nav-tabContent\">
                {% for infoUe in infoUes %}
                    <div class=\"tab-pane fade\" role=\"tabpanel\" id={{ 'list-' ~ infoUe.ue.code }} aria-labelledby={{ 'list-' ~ infoUe.ue.code ~ '-list' }}>
                        <div class=\"au-card\">
                            <span class=\"badge badge-pill badge-style2\">{{ infoUe.ue.semestre }}</span>
                            <span class=\"badge badge-pill badge-style\">{{ infoUe.ue.type }}</span>
                            <h5>
                                <strong>{{ infoUe.ue.code }}</strong>
                                <span>{{ infoUe.ue.nom }}</span>
                            </h5>
                            <hr>
                            <div class=\"row chartUe\" id={{ infoUe.ue.code }}></div>
                            <br>

                            <nav>
                                <div class=\"nav nav-tabs\" id={{ 'nav-tab' ~ infoUe.ue.code }} role=\"tablist\">
                                    <a class=\"nav-item nav-link active\" id={{ 'nav-objectif-tab' ~ infoUe.ue.code }} data-toggle=\"tab\" href={{ '#nav-objectif' ~ infoUe.ue.code }} role=\"tab\" aria-controls={{ 'nav-objectif' ~ infoUe.ue.code }} aria-selected=\"true\">Objectif</a>
                                    <a class=\"nav-item nav-link\" id={{ 'nav-programme-tab' ~ infoUe.ue.code }} data-toggle=\"tab\" href={{ '#nav-programme' ~ infoUe.ue.code }} role=\"tab\" aria-controls={{ 'nav-programme' ~ infoUe.ue.code }} aria-selected=\"false\">Programme</a>
                                    <a class=\"nav-item nav-link\" id={{ 'nav-competence-tab' ~ infoUe.ue.code }} data-toggle=\"tab\" href={{ '#nav-competence' ~ infoUe.ue.code }} role=\"tab\" aria-controls={{ 'nav-competence' ~ infoUe.ue.code }} aria-selected=\"false\">Compétences</a>
                                </div>
                            </nav>

                            <div class=\"tab-content\" id=\"nav-tabContent\">
                                <div class=\"tab-pane fade show active\" id={{ 'nav-objectif' ~ infoUe.ue.code }} role=\"tabpanel\" aria-labelledby={{ 'nav-objectif-tab' ~ infoUe.ue.code }}><br>{{ infoUe.ue.description }}</div>
                                <div class=\"tab-pane fade\" id={{ 'nav-programme' ~ infoUe.ue.code }} role=\"tabpanel\" aria-labelledby={{ 'nav-programme-tab' ~ infoUe.ue.code }}><br>{{ infoUe.ue.programme }}</div>
                                <div class=\"tab-pane fade\" id={{ 'nav-competence' ~ infoUe.ue.code }} role=\"tabpanel\" aria-labelledby={{ 'nav-competence-tab' ~ infoUe.ue.code }}><br>
                                    <div class=\"table-responsive\">
                                        <table class=\"table table-hover tableStyle\">
                                            <tbody>
                                            {% for microcompetence in infoUe.ue.microCompetences %}
                                                {% if microcompetence.sousCompetences.first  %}
                                                    <tr>
                                                        <th scope=\"row\"><span class=\"badge badge-pill badge-style2\">{{microcompetence.sousCompetences.first.competence.name}}</span></th>
                                                        <td>{{microcompetence.description}}</td>
                                                    </tr>
                                                {% endif %}
                                            {% endfor %}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                        createChart('{{ infoUe.ue.code }}',
                            {{ infoUe.competences.analyse }},
                            {{ infoUe.competences.conception }},
                            {{ infoUe.competences.realisation }},
                            {{ infoUe.competences.deploiement }},
                            {{ infoUe.competences.gestion }})
                    </script>
                {% else %}
                    <p>Aucun résulat</p>
                {% endfor %}
            </div>
        </div>
    </div>
</div>

{% endblock %}
", "catalogue/ues.html.twig", "/var/www/tx_back/competences_isi/templates/catalogue/ues.html.twig");
    }
}
