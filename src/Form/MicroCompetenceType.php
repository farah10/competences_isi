<?php

namespace App\Form;

use App\Entity\MicroCompetence;
use App\Entity\SousCompetence;
use App\Entity\Ue;
use App\Repository\SousCompetenceRepository;
use App\Repository\UeRepository;
use phpDocumentor\Reflection\DocBlock\Description;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MicroCompetenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('description', TextareaType::class, [
                'attr' => ['class' => 'form-control '],
                'row_attr' => ['class' => 'form-group'],
            ])
            ->add('sousCompetences', EntityType::class, [
                'row_attr' => ['class' => 'form-group'],
                'label' => false,
                'class' => SousCompetence::class,
                'choice_label' => 'description',
                'attr' => ['class'=>'form-check', 'required' => true],
                'query_builder' => function (SousCompetenceRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'ASC');
                },
                'choice_attr' => function($choice, $key, $value) {
                    $competenceName = $choice->getCompetence()->getName();
                    return ['class'=>'form-check-input testcheck '. $competenceName];
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('ues', EntityType::class, [
            'row_attr' => ['class' => 'form-group'],
            'label' => false,
            'class' => Ue::class,
            'choice_label' => function($choice, $key, $value) {
                return $choice->getCode().' || '.$choice->getNom();
            },
            'attr' => ['class'=>'form-check', 'required' => true],
            'query_builder' => function (UeRepository $er) {
                return $er->createQueryBuilder('u')
                    ->orderBy('u.id', 'ASC');
            },
            'choice_attr' => function($choice, $key, $value) {
                return ['class'=>'form-check-input testcheck'];
            },
            'multiple' => true,
            'expanded' => true,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MicroCompetence::class,
        ]);
    }
}
