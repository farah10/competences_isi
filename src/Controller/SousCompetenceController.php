<?php

namespace App\Controller;

use App\Entity\SousCompetence;
use App\Form\SousCompetenceType;
use App\Repository\SousCompetenceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sous/competence")
 */
class SousCompetenceController extends AbstractController
{
    /**
     * @Route("/new", name="sous_competence_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sousCompetence = new SousCompetence();
        $form = $this->createForm(SousCompetenceType::class, $sousCompetence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sousCompetence);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_sous_competence');
        }

        return $this->render('sous_competence/new.html.twig', [
            'sous_competence' => $sousCompetence,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sous_competence_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SousCompetence $sousCompetence): Response
    {
        $form = $this->createForm(SousCompetenceType::class, $sousCompetence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('app_admin_sous_competence');
        }

        return $this->render('sous_competence/edit.html.twig', [
            'sous_competence' => $sousCompetence,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sous_competence_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SousCompetence $sousCompetence): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sousCompetence->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sousCompetence);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_sous_competence');
    }
}
