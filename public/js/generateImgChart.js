function ImgChart (analyse,conception,realisation,deploiement,gestion){
    const options = {
        colors: ['#F46036', '#6f2b72', '#1B998B', '#E71D36', '#6B7C24'],
        fontName: 'poppins',
        chartArea:{top:20,left:50},
        pieHole: 0.4,
        legend: { position: 'bottom', alignment: 'right' },
        width:900,
        height:500,
    };

    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Competence', 'pourcentage par profil'],
            ['Analyse', analyse ],
            ['Conception', conception ],
            ['Réalisation', realisation ],
            ['Déploiement',deploiement ],
            ['Gestion', gestion ]
        ]);

        var chart = new google.visualization.PieChart(document.getElementById("chartProfil"));
        google.visualization.events.addListener(chart, 'ready', function () {
            var content = '<img src="' + chart.getImageURI() + '">'
            $('#elementImg').append(content)
            $('#imgSrc').append(chart.getImageURI())

        });
        chart.draw(data, options);
    }
}

