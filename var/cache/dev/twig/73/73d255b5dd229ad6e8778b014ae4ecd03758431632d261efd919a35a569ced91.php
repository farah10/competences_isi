<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_8e4c07a3bd7210428ea3905e885d36c2d5d192f12834f92ee30b248ae52a0c1d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
            'otherscripts' => [$this, 'block_otherscripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta charset=\"UTF-8\">
        <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/googleChart.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/jquery.js"), "html", null, true);
        echo "\"></script>
        ";
        // line 9
        $this->displayBlock('head', $context, $blocks);
        // line 10
        echo "
        <title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 20
        echo "    </head>
    <body>
        <div class=\"wrapper\">
            <!-- Sidebar  -->
            <nav id=\"sidebar\">
                <div class=\"sidebar-header\">
                    <h3><i class=\"fas fa-laptop\"></i> Profil ISI</h3>
                    <strong>ISI</strong>
                </div>

                <ul class=\"list-unstyled components\">
                    <li>
                        <a href=";
        // line 32
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_accueil");
        echo ">
                           <i class=\"fas fa-home\"></i>
                            Accueil
                        </a>
                    </li>
                    <li>
                        <a href=\"#catalogueSubmenu\" data-toggle=\"collapse\" aria-expanded=\"false\" class=\"dropdown-toggle\">
                            <i class=\"fas fa-copy\"></i>
                            Catalogue
                        </a>
                        <ul class=\"collapse list-unstyled\" id=\"catalogueSubmenu\">
                            <li>
                                <a href=";
        // line 44
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_catalogue_ues");
        echo ">UE</a>
                            </li>
                            <li>
                                <a href=";
        // line 47
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_catalogue_competences");
        echo ">Compétences</a>
                            </li>
                        </ul>
                    </li>
                    ";
        // line 51
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
            // line 52
            echo "                        <li>
                            <a href=";
            // line 53
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_profil");
            echo ">
                                <i class=\"fas fa-graduation-cap\"></i>
                                Mon profil
                            </a>
                        </li>
                        <li>
                            <a href=";
            // line 59
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_choixUe");
            echo ">
                                <i class=\"fas fa-briefcase\"></i>
                                Choisir mes ues
                            </a>
                        </li>
                        <li>
                            <a href=";
            // line 65
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo ">
                                <i class=\"fas fa-sign-out-alt\"></i>
                                Se déconnecter
                            </a>
                        </li>
                    ";
        } elseif ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 71
            echo "                        <li>
                            <a href=";
            // line 72
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin");
            echo ">
                                <i class=\"fas fa-graduation-cap\"></i>
                                Administrer
                            </a>
                        </li>
                        <li>
                            <a href=";
            // line 78
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_ue");
            echo ">
                                <i class=\"fas fa-edit\"></i>
                                Editer Ues
                            </a>
                        </li>
                        <li>
                            <a href=";
            // line 84
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_sous_competence");
            echo ">
                                <i class=\"fas fa-edit\"></i>
                                Editer sous compétences
                            </a>
                        </li>
                        <li>
                            <a href=";
            // line 90
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin_micro_competence");
            echo ">
                                <i class=\"fas fa-edit\"></i>
                                Editer micro compétences
                            </a>
                        </li>
                        <li>
                            <a href=";
            // line 96
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo ">
                                <i class=\"fas fa-sign-out-alt\"></i>
                                Se déconnecter
                            </a>
                        </li>
                    ";
        } else {
            // line 102
            echo "                        <li>
                            <a href=";
            // line 103
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_login");
            echo ">
                                <i class=\"fas fa-graduation-cap\"></i>
                                Se connecter
                            </a>
                        </li>
                        <li>
                            <a href=";
            // line 109
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_register");
            echo ">
                                <i class=\"fas fa-briefcase\"></i>
                                S'inscrire
                            </a>
                        </li>
                    ";
        }
        // line 115
        echo "                </ul>
            </nav>
            <div id=\"content\">
                <button type=\"button\" id=\"sidebarCollapse\" class=\"btn\">
                    <i class=\"fas fa-align-justify\"></i>
                </button>
                ";
        // line 121
        $this->displayBlock('body', $context, $blocks);
        // line 122
        echo "            </div>
        </div>

        ";
        // line 125
        $this->displayBlock('javascripts', $context, $blocks);
        // line 132
        echo "
        ";
        // line 133
        $this->displayBlock('otherscripts', $context, $blocks);
        // line 134
        echo "    </body>
</html>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 9
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 11
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Profil ISI";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 14
        echo "            <link rel=\"stylesheet\" href=";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo ">
            <link rel=\"stylesheet\" href=";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/style.css"), "html", null, true);
        echo ">
            <link rel=\"stylesheet\" href=";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/tab.css"), "html", null, true);
        echo ">
            <link rel=\"stylesheet\" href=";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/accordion.css"), "html", null, true);
        echo ">
            <link rel=\"stylesheet\" href=";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/form.css"), "html", null, true);
        echo ">
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 121
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 125
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 126
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 127
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/popper.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script>
            <script defer src=\"https://use.fontawesome.com/releases/v5.0.13/js/solid.js\" integrity=\"sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ\" crossorigin=\"anonymous\"></script>
            <script defer src=\"https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js\" integrity=\"sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY\" crossorigin=\"anonymous\"></script>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 133
    public function block_otherscripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "otherscripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  345 => 133,  334 => 128,  330 => 127,  325 => 126,  318 => 125,  306 => 121,  297 => 18,  293 => 17,  289 => 16,  285 => 15,  280 => 14,  273 => 13,  260 => 11,  248 => 9,  239 => 134,  237 => 133,  234 => 132,  232 => 125,  227 => 122,  225 => 121,  217 => 115,  208 => 109,  199 => 103,  196 => 102,  187 => 96,  178 => 90,  169 => 84,  160 => 78,  151 => 72,  148 => 71,  139 => 65,  130 => 59,  121 => 53,  118 => 52,  116 => 51,  109 => 47,  103 => 44,  88 => 32,  74 => 20,  72 => 13,  67 => 11,  64 => 10,  62 => 9,  58 => 8,  54 => 7,  46 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta charset=\"UTF-8\">
        <script src=\"{{ asset('js/googleChart.js')}}\"></script>
        <script src=\"{{ asset('js/jquery.js')}}\"></script>
        {% block head %}{% endblock %}

        <title>{% block title %}Profil ISI{% endblock %}</title>

        {% block stylesheets %}
            <link rel=\"stylesheet\" href={{ asset(\"css/bootstrap.min.css\") }}>
            <link rel=\"stylesheet\" href={{ asset(\"css/style.css\") }}>
            <link rel=\"stylesheet\" href={{ asset(\"css/tab.css\") }}>
            <link rel=\"stylesheet\" href={{ asset(\"css/accordion.css\") }}>
            <link rel=\"stylesheet\" href={{ asset(\"css/form.css\") }}>
        {% endblock %}
    </head>
    <body>
        <div class=\"wrapper\">
            <!-- Sidebar  -->
            <nav id=\"sidebar\">
                <div class=\"sidebar-header\">
                    <h3><i class=\"fas fa-laptop\"></i> Profil ISI</h3>
                    <strong>ISI</strong>
                </div>

                <ul class=\"list-unstyled components\">
                    <li>
                        <a href={{ path('app_accueil') }}>
                           <i class=\"fas fa-home\"></i>
                            Accueil
                        </a>
                    </li>
                    <li>
                        <a href=\"#catalogueSubmenu\" data-toggle=\"collapse\" aria-expanded=\"false\" class=\"dropdown-toggle\">
                            <i class=\"fas fa-copy\"></i>
                            Catalogue
                        </a>
                        <ul class=\"collapse list-unstyled\" id=\"catalogueSubmenu\">
                            <li>
                                <a href={{ path('app_catalogue_ues') }}>UE</a>
                            </li>
                            <li>
                                <a href={{ path('app_catalogue_competences') }}>Compétences</a>
                            </li>
                        </ul>
                    </li>
                    {% if is_granted(\"ROLE_USER\") %}
                        <li>
                            <a href={{ path('app_profil') }}>
                                <i class=\"fas fa-graduation-cap\"></i>
                                Mon profil
                            </a>
                        </li>
                        <li>
                            <a href={{ path('app_choixUe') }}>
                                <i class=\"fas fa-briefcase\"></i>
                                Choisir mes ues
                            </a>
                        </li>
                        <li>
                            <a href={{ path('app_logout') }}>
                                <i class=\"fas fa-sign-out-alt\"></i>
                                Se déconnecter
                            </a>
                        </li>
                    {% elseif is_granted(\"ROLE_ADMIN\") %}
                        <li>
                            <a href={{ path('app_admin') }}>
                                <i class=\"fas fa-graduation-cap\"></i>
                                Administrer
                            </a>
                        </li>
                        <li>
                            <a href={{ path('app_admin_ue') }}>
                                <i class=\"fas fa-edit\"></i>
                                Editer Ues
                            </a>
                        </li>
                        <li>
                            <a href={{ path('app_admin_sous_competence') }}>
                                <i class=\"fas fa-edit\"></i>
                                Editer sous compétences
                            </a>
                        </li>
                        <li>
                            <a href={{ path('app_admin_micro_competence') }}>
                                <i class=\"fas fa-edit\"></i>
                                Editer micro compétences
                            </a>
                        </li>
                        <li>
                            <a href={{ path('app_logout') }}>
                                <i class=\"fas fa-sign-out-alt\"></i>
                                Se déconnecter
                            </a>
                        </li>
                    {% else %}
                        <li>
                            <a href={{ path('app_login') }}>
                                <i class=\"fas fa-graduation-cap\"></i>
                                Se connecter
                            </a>
                        </li>
                        <li>
                            <a href={{ path('app_register') }}>
                                <i class=\"fas fa-briefcase\"></i>
                                S'inscrire
                            </a>
                        </li>
                    {% endif %}
                </ul>
            </nav>
            <div id=\"content\">
                <button type=\"button\" id=\"sidebarCollapse\" class=\"btn\">
                    <i class=\"fas fa-align-justify\"></i>
                </button>
                {% block body %}{% endblock %}
            </div>
        </div>

        {% block javascripts %}
            <script src=\"{{ asset('js/bootstrap.min.js')}}\"></script>
            <script src=\"{{ asset('js/popper.min.js')}}\"></script>
            <script src=\"{{ asset('js/main.js')}}\"></script>
            <script defer src=\"https://use.fontawesome.com/releases/v5.0.13/js/solid.js\" integrity=\"sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ\" crossorigin=\"anonymous\"></script>
            <script defer src=\"https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js\" integrity=\"sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY\" crossorigin=\"anonymous\"></script>
        {% endblock %}

        {% block otherscripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/var/www/tx_back/competences_isi/templates/base.html.twig");
    }
}
