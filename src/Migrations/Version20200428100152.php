<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200428100152 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE micro_competence (id INT AUTO_INCREMENT NOT NULL, sous_competence_id_id INT NOT NULL, description VARCHAR(255) NOT NULL, INDEX IDX_4366FA8B1AE938F (sous_competence_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sous_competence (id INT AUTO_INCREMENT NOT NULL, competence_id_id INT NOT NULL, description VARCHAR(255) NOT NULL, INDEX IDX_72AC3D95F566CFB1 (competence_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE competence (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, complete_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ue (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, semestre VARCHAR(255) DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ue_micro_competence (ue_id INT NOT NULL, micro_competence_id INT NOT NULL, INDEX IDX_FB65A7F562E883B1 (ue_id), INDEX IDX_FB65A7F59CF52B1D (micro_competence_id), PRIMARY KEY(ue_id, micro_competence_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE micro_competence ADD CONSTRAINT FK_4366FA8B1AE938F FOREIGN KEY (sous_competence_id_id) REFERENCES sous_competence (id)');
        $this->addSql('ALTER TABLE sous_competence ADD CONSTRAINT FK_72AC3D95F566CFB1 FOREIGN KEY (competence_id_id) REFERENCES competence (id)');
        $this->addSql('ALTER TABLE ue_micro_competence ADD CONSTRAINT FK_FB65A7F562E883B1 FOREIGN KEY (ue_id) REFERENCES ue (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ue_micro_competence ADD CONSTRAINT FK_FB65A7F59CF52B1D FOREIGN KEY (micro_competence_id) REFERENCES micro_competence (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ue_micro_competence DROP FOREIGN KEY FK_FB65A7F59CF52B1D');
        $this->addSql('ALTER TABLE micro_competence DROP FOREIGN KEY FK_4366FA8B1AE938F');
        $this->addSql('ALTER TABLE sous_competence DROP FOREIGN KEY FK_72AC3D95F566CFB1');
        $this->addSql('ALTER TABLE ue_micro_competence DROP FOREIGN KEY FK_FB65A7F562E883B1');
        $this->addSql('DROP TABLE micro_competence');
        $this->addSql('DROP TABLE sous_competence');
        $this->addSql('DROP TABLE competence');
        $this->addSql('DROP TABLE ue');
        $this->addSql('DROP TABLE ue_micro_competence');
        $this->addSql('DROP TABLE user');
    }
}
