<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SousCompetenceRepository")
 */
class SousCompetence
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Competence", inversedBy="sousCompetences")
     * @ORM\JoinColumn(nullable=false)
     */
    private $competence;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MicroCompetence", inversedBy="sousCompetences")
     */
    private $microCompetences;

    public function __construct()
    {
        $this->microCompetences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCompetence(): ?Competence
    {
        return $this->competence;
    }

    public function setCompetence(?Competence $competence): self
    {
        $this->competence = $competence;

        return $this;
    }

    /**
     * @return Collection|MicroCompetence[]
     */
    public function getMicroCompetences(): Collection
    {
        return $this->microCompetences;
    }

    public function addMicroCompetence(MicroCompetence $microCompetence): self
    {
        if (!$this->microCompetences->contains($microCompetence)) {
            $this->microCompetences[] = $microCompetence;
        }

        return $this;
    }

    public function removeMicroCompetence(MicroCompetence $microCompetence): self
    {
        if ($this->microCompetences->contains($microCompetence)) {
            $this->microCompetences->removeElement($microCompetence);
        }

        return $this;
    }
}
