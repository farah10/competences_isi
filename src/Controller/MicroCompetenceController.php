<?php

namespace App\Controller;

use App\Entity\MicroCompetence;
use App\Entity\SousCompetence;
use App\Entity\Ue;
use App\Form\MicroCompetenceType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/micro/competence")
 */
class MicroCompetenceController extends AbstractController
{
    /**
     * @Route("/new", name="micro_competence_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $microCompetence = new MicroCompetence();
        $form = $this->createForm(MicroCompetenceType::class, $microCompetence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($microCompetence);
            $entityManager->flush();

            // TODO Trouver une meilleure façon d'enregister la microCompétence dans la sousCompétence associée.
            $mc = $entityManager->getRepository(MicroCompetence::class)->findOneBy(array("description"=>$microCompetence->getDescription()));
            foreach($microCompetence->getSousCompetences() as $sousComp){
                $id = $sousComp->getId();
                $sousCompetence = $entityManager->getRepository(SousCompetence::class)->find($id);;
                $sousCompetence->addMicroCompetence($mc);
                $entityManager->persist($sousCompetence);
                $entityManager->flush();
            };

            // TODO Trouver une meilleure façon d'associée des UEs à la microcompétence.
            foreach($microCompetence->getUes() as $ue){
                $id = $ue->getId();
                $u = $entityManager->getRepository(Ue::class)->find($id);;
                $u->addMicroCompetence($mc);
                $entityManager->persist($u);
                $entityManager->flush();
            };

            return $this->redirectToRoute('app_admin');
        }

        return $this->render('micro_competence/new.html.twig', [
            'micro_competence' => $microCompetence,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="micro_competence_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, MicroCompetence $microCompetence): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $form = $this->createForm(MicroCompetenceType::class, $microCompetence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $micro = $entityManager->getRepository(MicroCompetence::class)->find($microCompetence->getId());

            $allSousComp = $entityManager->getRepository(SousCompetence::class)->findAll();
            foreach($allSousComp as $sousCompetence){
                $sousComp = $entityManager->getRepository(SousCompetence::class)->find($sousCompetence->getId());
                $sousComp->removeMicroCompetence($micro);
                $entityManager->persist($sousComp);
            };

            foreach($form->getData()->getSousCompetences() as $sousCompetence){
                $sousComp = $entityManager->getRepository(SousCompetence::class)->find($sousCompetence->getId());
                $sousComp->addMicroCompetence($microCompetence);
                $entityManager->persist($sousComp);
                $entityManager->flush();
            };

            $allUe = $entityManager->getRepository(Ue::class)->findAll();
            foreach($allUe as $ue){
                $u = $entityManager->getRepository(Ue::class)->find($ue->getId());
                $u->removeMicroCompetence($micro);
                $entityManager->persist($u);
            };

            foreach($form->getData()->getUes() as $ue){
                $u = $entityManager->getRepository(Ue::class)->find($ue->getId());
                $u->addMicroCompetence($microCompetence);
                $entityManager->persist($u);
                $entityManager->flush();
            };


            return $this->redirectToRoute('app_admin_micro_competence');
        }

        return $this->render('micro_competence/edit.html.twig', [
            'micro_competence' => $microCompetence,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="micro_competence_delete", methods={"DELETE"})
     */
    public function delete(Request $request, MicroCompetence $microCompetence): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        if ($this->isCsrfTokenValid('delete'.$microCompetence->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($microCompetence);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin');
    }
}
