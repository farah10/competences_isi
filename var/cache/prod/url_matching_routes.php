<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/admin' => [[['_route' => 'app_admin', '_controller' => 'App\\Controller\\AdminController::index'], null, null, null, false, false, null]],
        '/catalogue/competences' => [[['_route' => 'app_catalogue_competences', '_controller' => 'App\\Controller\\CatalogueController::competences'], null, null, null, false, false, null]],
        '/catalogue/ues' => [[['_route' => 'app_catalogue_ues', '_controller' => 'App\\Controller\\CatalogueController::ues'], null, null, null, false, false, null]],
        '/choixUE' => [[['_route' => 'app_choixUe', '_controller' => 'App\\Controller\\ChoixUeController::index'], null, null, null, false, false, null]],
        '/competence' => [[['_route' => 'competence', '_controller' => 'App\\Controller\\CompetenceController::index'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'app_accueil', '_controller' => 'App\\Controller\\HomeController::index'], null, null, null, false, false, null]],
        '/micro/competence/new' => [[['_route' => 'micro_competence_new', '_controller' => 'App\\Controller\\MicroCompetenceController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/profil' => [[['_route' => 'app_profil', '_controller' => 'App\\Controller\\ProfilController::index'], null, null, null, false, false, null]],
        '/profil/pdf' => [[['_route' => 'app_pdf', '_controller' => 'App\\Controller\\ProfilController::generate_pdf'], null, ['POST' => 0], null, false, false, null]],
        '/register' => [[['_route' => 'app_register', '_controller' => 'App\\Controller\\RegistrationController::register'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/sous/competence/new' => [[['_route' => 'sous_competence_new', '_controller' => 'App\\Controller\\SousCompetenceController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/ue/new' => [[['_route' => 'ue_new', '_controller' => 'App\\Controller\\UeController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/micro/competence/([^/]++)(?'
                    .'|/edit(*:41)'
                    .'|(*:48)'
                .')'
                .'|/sous/competence/([^/]++)(?'
                    .'|/edit(*:89)'
                    .'|(*:96)'
                .')'
                .'|/ue/([^/]++)(?'
                    .'|/edit(*:124)'
                    .'|(*:132)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        41 => [[['_route' => 'micro_competence_edit', '_controller' => 'App\\Controller\\MicroCompetenceController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        48 => [[['_route' => 'micro_competence_delete', '_controller' => 'App\\Controller\\MicroCompetenceController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        89 => [[['_route' => 'sous_competence_edit', '_controller' => 'App\\Controller\\SousCompetenceController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        96 => [[['_route' => 'sous_competence_delete', '_controller' => 'App\\Controller\\SousCompetenceController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        124 => [[['_route' => 'ue_edit', '_controller' => 'App\\Controller\\UeController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        132 => [
            [['_route' => 'ue_delete', '_controller' => 'App\\Controller\\UeController::delete'], ['id'], ['DELETE' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
