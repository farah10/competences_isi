<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200614094021 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE micro_competence_sous_competence (micro_competence_id INT NOT NULL, sous_competence_id INT NOT NULL, INDEX IDX_B2BD30559CF52B1D (micro_competence_id), INDEX IDX_B2BD305528111824 (sous_competence_id), PRIMARY KEY(micro_competence_id, sous_competence_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE micro_competence_sous_competence ADD CONSTRAINT FK_B2BD30559CF52B1D FOREIGN KEY (micro_competence_id) REFERENCES micro_competence (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE micro_competence_sous_competence ADD CONSTRAINT FK_B2BD305528111824 FOREIGN KEY (sous_competence_id) REFERENCES sous_competence (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE sous_competence_micro_competence');
        $this->addSql('ALTER TABLE note CHANGE lettre lettre VARCHAR(5) DEFAULT NULL');
        $this->addSql('ALTER TABLE ue CHANGE semestre semestre VARCHAR(255) DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT NULL, CHANGE filliere filliere VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sous_competence_micro_competence (sous_competence_id INT NOT NULL, micro_competence_id INT NOT NULL, INDEX IDX_8460B3969CF52B1D (micro_competence_id), INDEX IDX_8460B39628111824 (sous_competence_id), PRIMARY KEY(sous_competence_id, micro_competence_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE sous_competence_micro_competence ADD CONSTRAINT FK_8460B39628111824 FOREIGN KEY (sous_competence_id) REFERENCES sous_competence (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sous_competence_micro_competence ADD CONSTRAINT FK_8460B3969CF52B1D FOREIGN KEY (micro_competence_id) REFERENCES micro_competence (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE micro_competence_sous_competence');
        $this->addSql('ALTER TABLE note CHANGE lettre lettre VARCHAR(5) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE ue CHANGE semestre semestre VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE type type VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE filliere filliere VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin');
    }
}
