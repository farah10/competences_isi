<?php

namespace App\Controller;

use App\Entity\Competence;
use App\Entity\Note;
use App\Form\AddUeType;
use App\Service\UeService;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ProfilController extends AbstractController
{
    private $ueService;
    private $security;

    public function __construct(UeService $ueService, Security $security)
    {
        $this->ueService = $ueService;
        $this->security = $security;
    }

    /**
     * @Route("/profil", name="app_profil")
     */
    public function index(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user = $this->security->getUser();

        $uesSuivies = $user->getUes();

        $notes = $this->getDoctrine()
            ->getRepository(Note::class)->findBy(array('user' => $user));

        $competences = $competences = $this->getDoctrine()
            ->getRepository(Competence::class)->findAll();

        $profilCompetences = $this->ueService->getProfilCompetence($uesSuivies);

        $form = $this->createForm(AddUeType::class);
        $form->handleRequest($request);

        $microCompetences = $this->ueService->getMicroCompetenceFromProfil($competences,($uesSuivies));

        if ($form->isSubmitted() && $form->isValid()) {
            $ue = $form->getData();
            $user->addUe($ue['ue']);

            $note = new Note();
            $note->setUe($ue['ue']);
            $note->setLettre($ue['lettre']);
            $note->setUser($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->persist($note);
            $entityManager->flush();

            $profilCompetences = $this->ueService->getProfilCompetence($uesSuivies);
            $microCompetences = $this->ueService->getMicroCompetenceFromProfil($competences,($uesSuivies));
            $notes = $this->getDoctrine()
                ->getRepository(Note::class)->findBy(array('user' => $user));

            return $this->render('profil.html.twig', [
                'user'=> $user,
                'notes'=> $notes,
                'microCompetences' => $microCompetences,
                'profilCompetences'=> $profilCompetences,
                'form' => $form->createView(),
            ]);
        }

        return $this->render('profil.html.twig', [
            'user'=> $user,
            'notes'=> $notes,
            'microCompetences' => $microCompetences,
            'profilCompetences'=> $profilCompetences,
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/profil/pdf", name="app_pdf", methods={"POST"})
     * @param Request $request
     */
    public function generate_pdf(Request $request){

        $this->denyAccessUnlessGranted('ROLE_USER');

        $user = $this->security->getUser();
        $competences = $this->getDoctrine()
            ->getRepository(Competence::class)->findAll();
        $microCompetences = $this->ueService->getMicroCompetenceFromProfil($competences,($user->getUes()));
        $notes = $this->getDoctrine()
            ->getRepository(Note::class)->findBy(array('user' => $user));

        // Source de l'image du google chart
        $srcChart = $request->get('src');

        $options = new Options();
        $options->set('defaultFont', 'Roboto');
        $options->set('isRemoteEnabled', TRUE);

        $dompdf = new Dompdf($options);

        $data = array(
            'user' => $user,
            'microCompetences' => $microCompetences,
            'notes' => $notes,
            'srcChart' => $srcChart
        );

        $html = $this->renderView('pdf.html.twig', $data);

        // Autorise la génération d'image
        $contxt = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed'=> TRUE
            ]
        ]);

        $dompdf->setHttpContext($contxt);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream("MonProfil.pdf", [
            "Attachment" => true
        ]);

    }

    /**
     * @Route("profil/{id}", name="app_ue_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Note $note): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        if ($this->isCsrfTokenValid('delete'.$note->getId(), $request->request->get('_token'))) {
            $userId = $note->getUser()->getId();
            $ueId = $note->getUe()->getId();

            // Supprimer également l'ue dans la relation d'UE suivies
            $conn = $this->getDoctrine()->getConnection();
            $sql = 'DELETE FROM ue_user WHERE user_id = :userId AND ue_id = :ueId';
            $stmt = $conn->prepare($sql);
            $stmt->execute(['userId' => $userId, 'ueId' => $ueId]);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($note);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_profil');
    }


}
