<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* profil.html.twig */
class __TwigTemplate_8cc9f46bb825eb2d4de327f68e02eec2b1c3b89fbff143127204c956e2112992 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "profil.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "profil.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Mon profil";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head"));

        // line 6
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/generateImgChart.js"), "html", null, true);
        echo "\"></script>
    <script>
        ImgChart(";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["profilCompetences"]) || array_key_exists("profilCompetences", $context) ? $context["profilCompetences"] : (function () { throw new RuntimeError('Variable "profilCompetences" does not exist.', 8, $this->source); })()), "analyse", [], "any", false, false, false, 8), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["profilCompetences"]) || array_key_exists("profilCompetences", $context) ? $context["profilCompetences"] : (function () { throw new RuntimeError('Variable "profilCompetences" does not exist.', 8, $this->source); })()), "conception", [], "any", false, false, false, 8), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["profilCompetences"]) || array_key_exists("profilCompetences", $context) ? $context["profilCompetences"] : (function () { throw new RuntimeError('Variable "profilCompetences" does not exist.', 8, $this->source); })()), "realisation", [], "any", false, false, false, 8), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["profilCompetences"]) || array_key_exists("profilCompetences", $context) ? $context["profilCompetences"] : (function () { throw new RuntimeError('Variable "profilCompetences" does not exist.', 8, $this->source); })()), "deploiement", [], "any", false, false, false, 8), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["profilCompetences"]) || array_key_exists("profilCompetences", $context) ? $context["profilCompetences"] : (function () { throw new RuntimeError('Variable "profilCompetences" does not exist.', 8, $this->source); })()), "gestion", [], "any", false, false, false, 8), "html", null, true);
        echo "  )
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 12
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 13
        echo "<div class=\"tab_container\">
    <h1>Mon profil</h1>
    <hr class=\"my-4\">

    ";
        // line 18
        echo "    <div id=\"elementImg\" style=\"display:none\"></div>
    <div id=\"imgSrc\" style=\"display:none\"></div>
    <form id=\"imgData\" method=\"post\" action=";
        // line 20
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_pdf");
        echo ">
        <input type=\"hidden\" name=\"src\" id=\"src\"/>
    </form>

    <div class=\"row\">
        <div class=\"au-card col-md-4 col-sm-12 profil\">
            <table class=\"tableProfil table table-sm\">
                <thead>
                <tr>
                    <th scope=\"col\">#</th>
                    <th scope=\"col\">UE</th>
                    <th scope=\"col\">Note</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                ";
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["notes"]) || array_key_exists("notes", $context) ? $context["notes"] : (function () { throw new RuntimeError('Variable "notes" does not exist.', 36, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["note"]) {
            // line 37
            echo "                <tr>
                    <th scope=\"row\">";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["note"], "ue", [], "any", false, false, false, 38), "code", [], "any", false, false, false, 38), "html", null, true);
            echo "</th>
                    <td>";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["note"], "ue", [], "any", false, false, false, 39), "nom", [], "any", false, false, false, 39), "html", null, true);
            echo "</td>
                    <td colspan=\"1\"><span class=\"badge badge-pill badge-style\">";
            // line 40
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["note"], "lettre", [], "any", false, false, false, 40), "html", null, true);
            echo "</span></td>
                    <td>
                        <form method=\"post\" onsubmit=\"deleteMessage()\" action=";
            // line 42
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_ue_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["note"], "id", [], "any", false, false, false, 42)]), "html", null, true);
            echo ">
                            <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
                            <input type=\"hidden\" name=\"_token\" value=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(("delete" . twig_get_attribute($this->env, $this->source, $context["note"], "id", [], "any", false, false, false, 44))), "html", null, true);
            echo "\">
                            <button type=\"submit\" class=\"btn btn-danger btn-sm\"><i class=\"fas fa-trash\"></i></button>
                        </form>
                    </td>
                </tr>
                </tbody>
                ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 51
            echo "                    <p>Aucune Ue suivie</p>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['note'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "            </table>
            <div class=\"line\"></div>
            <div>
                <h6>Ajouter une UE suivie</h6>
                ";
        // line 67
        if ((isset($context["form"]) || array_key_exists("form", $context))) {
            // line 68
            echo "                ";
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 68, $this->source); })()), 'form_start');
            echo "
                <div class=\"form-label-group\">
                    ";
            // line 70
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 70, $this->source); })()), 'widget');
            echo "
                </div>
                <button class=\"btn btn-style-2\">";
            // line 72
            echo twig_escape_filter($this->env, (((isset($context["button_label"]) || array_key_exists("button_label", $context))) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 72, $this->source); })()), "Ajouter")) : ("Ajouter")), "html", null, true);
            echo "</button>
                ";
            // line 73
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 73, $this->source); })()), 'form_end');
            echo "
                ";
        }
        // line 75
        echo "            </div>
            <a id=\"generatePDF\"><button onclick=\"generatePDF()\" class=\"btn btn-style-2 down\">Télécharger mon profil en PDF</button></a>
        </div>
        <div class=\"au-card col-md-7 col-sm-12 profil\">
            <h5 class=\"float-right\">";
        // line 79
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 79, $this->source); })()), "firstname", [], "any", false, false, false, 79), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 79, $this->source); })()), "lastname", [], "any", false, false, false, 79), "html", null, true);
        echo "</h5>
            <br>
            <nav>
                <div class=\"nav nav-tabs\" id=\"chart\" role=\"tablist\">
                    <a class=\"nav-item nav-link active\" id=\"nav-chart-tab\" data-toggle=\"tab\" href=\"#nav-chart\" role=\"tab\" aria-controls=\"nav-chart\" aria-selected=\"true\">Profil</a>
                    <a class=\"nav-item nav-link\" id=\"nav-competence-tab\" data-toggle=\"tab\" href=\"#nav-competence\" role=\"tab\" aria-controls=\"nav-competence\" aria-selected=\"false\">Compétences</a>
               </div>
            </nav>

            <div class=\"tab-content\" id=\"nav-tabContent\">
                <div class=\"tab-pane fade show active\" id=\"nav-chart\" role=\"tabpanel\" aria-labelledby=\"nav-chart-tab\">
                    <div id=\"chartProfil\"></div>
                </div>
                <div class=\"tab-pane fade\" id=\"nav-competence\" role=\"tabpanel\" aria-labelledby=\"nav-competence-tab\">
                    <h6>Séléctionner la compétence que vous souhaitez consulter</h6>
                    <input class=\"inputTab\" id=\"tab1\" type=\"radio\" name=\"tabs\" checked>
                    <label class=\"competence\" for=\"tab1\"><span>Analyse</span></label>

                    <input class=\"inputTab\" id=\"tab2\" type=\"radio\" name=\"tabs\">
                    <label class=\"competence\" for=\"tab2\"><span>Conception</span></label>

                    <input class=\"inputTab\" id=\"tab3\" type=\"radio\" name=\"tabs\">
                    <label class=\"competence\" for=\"tab3\"><span>Réalisation</span></label>

                    <input class=\"inputTab\" id=\"tab4\" type=\"radio\" name=\"tabs\">
                    <label class=\"competence\" for=\"tab4\"><span>Déploiement</span></label>

                    <input class=\"inputTab\" id=\"tab5\" type=\"radio\" name=\"tabs\">
                    <label class=\"competence\" for=\"tab5\"><span>Gestion</span></label>

                    <section id=\"content1\" class=\"tab-content\">
                        <table id=\"table1\" class=\"table table-sm tableProfil\">
                            ";
        // line 111
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["microCompetences"]) || array_key_exists("microCompetences", $context) ? $context["microCompetences"] : (function () { throw new RuntimeError('Variable "microCompetences" does not exist.', 111, $this->source); })()), "analyse", [], "array", false, false, false, 111));
        foreach ($context['_seq'] as $context["_key"] => $context["microcompetence"]) {
            // line 112
            echo "                                <tr><td>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["microcompetence"], "description", [], "any", false, false, false, 112), "html", null, true);
            echo "</td></tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['microcompetence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 114
        echo "                        </table>
                    </section>

                    <section id=\"content2\" class=\"tab-content\">
                        <table id=\"table2\" class=\"table table-sm tableProfil\">
                            ";
        // line 119
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["microCompetences"]) || array_key_exists("microCompetences", $context) ? $context["microCompetences"] : (function () { throw new RuntimeError('Variable "microCompetences" does not exist.', 119, $this->source); })()), "conception", [], "array", false, false, false, 119));
        foreach ($context['_seq'] as $context["_key"] => $context["microcompetence"]) {
            // line 120
            echo "                                <tr><td>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["microcompetence"], "description", [], "any", false, false, false, 120), "html", null, true);
            echo "</td></tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['microcompetence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 122
        echo "                        </table>
                    </section>

                    <section id=\"content3\" class=\"tab-content\">
                        <table id=\"table3\" class=\"table table-sm tableProfil\">
                            ";
        // line 127
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["microCompetences"]) || array_key_exists("microCompetences", $context) ? $context["microCompetences"] : (function () { throw new RuntimeError('Variable "microCompetences" does not exist.', 127, $this->source); })()), "realisation", [], "array", false, false, false, 127));
        foreach ($context['_seq'] as $context["_key"] => $context["microcompetence"]) {
            // line 128
            echo "                                <tr><td>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["microcompetence"], "description", [], "any", false, false, false, 128), "html", null, true);
            echo "</td></tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['microcompetence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 130
        echo "                        </table>
                    </section>

                    <section id=\"content4\" class=\"tab-content\">
                        <table id=\"table4\" class=\"table table-sm tableProfil\">
                            ";
        // line 135
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["microCompetences"]) || array_key_exists("microCompetences", $context) ? $context["microCompetences"] : (function () { throw new RuntimeError('Variable "microCompetences" does not exist.', 135, $this->source); })()), "deploiement", [], "array", false, false, false, 135));
        foreach ($context['_seq'] as $context["_key"] => $context["microcompetence"]) {
            // line 136
            echo "                                <tr><td>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["microcompetence"], "description", [], "any", false, false, false, 136), "html", null, true);
            echo "</td></tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['microcompetence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 138
        echo "                        </table>
                    </section>

                    <section id=\"content5\" class=\"tab-content\">
                        <table id=\"table5\" class=\"table table-sm tableProfil\">
                            ";
        // line 143
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["microCompetences"]) || array_key_exists("microCompetences", $context) ? $context["microCompetences"] : (function () { throw new RuntimeError('Variable "microCompetences" does not exist.', 143, $this->source); })()), "gestion", [], "array", false, false, false, 143));
        foreach ($context['_seq'] as $context["_key"] => $context["microcompetence"]) {
            // line 144
            echo "                                <tr><td>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["microcompetence"], "description", [], "any", false, false, false, 144), "html", null, true);
            echo "</td></tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['microcompetence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 146
        echo "                        </table>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
      function generatePDF(){
          document.getElementById('src').value = document.getElementById('imgSrc').innerHTML;
          document.getElementById('imgData').submit();
      }
      function deleteMessage(){
          return confirm('Voulez vraiment suppprimer cet élément?');
      }
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "profil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  343 => 146,  334 => 144,  330 => 143,  323 => 138,  314 => 136,  310 => 135,  303 => 130,  294 => 128,  290 => 127,  283 => 122,  274 => 120,  270 => 119,  263 => 114,  254 => 112,  250 => 111,  213 => 79,  207 => 75,  202 => 73,  198 => 72,  193 => 70,  187 => 68,  185 => 67,  179 => 63,  172 => 51,  160 => 44,  155 => 42,  150 => 40,  146 => 39,  142 => 38,  139 => 37,  134 => 36,  115 => 20,  111 => 18,  105 => 13,  98 => 12,  80 => 8,  74 => 6,  67 => 5,  54 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Mon profil{% endblock %}

{% block head %}
    <script src=\"{{ asset('js/generateImgChart.js')}}\"></script>
    <script>
        ImgChart({{ profilCompetences.analyse }}, {{ profilCompetences.conception }}, {{ profilCompetences.realisation }}, {{ profilCompetences.deploiement }}, {{ profilCompetences.gestion }}  )
    </script>
{% endblock %}

{% block body %}
<div class=\"tab_container\">
    <h1>Mon profil</h1>
    <hr class=\"my-4\">

    {# Form pour générer le chart en pdf depuis son image en png #}
    <div id=\"elementImg\" style=\"display:none\"></div>
    <div id=\"imgSrc\" style=\"display:none\"></div>
    <form id=\"imgData\" method=\"post\" action={{ path('app_pdf')}}>
        <input type=\"hidden\" name=\"src\" id=\"src\"/>
    </form>

    <div class=\"row\">
        <div class=\"au-card col-md-4 col-sm-12 profil\">
            <table class=\"tableProfil table table-sm\">
                <thead>
                <tr>
                    <th scope=\"col\">#</th>
                    <th scope=\"col\">UE</th>
                    <th scope=\"col\">Note</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {% for note in notes %}
                <tr>
                    <th scope=\"row\">{{ note.ue.code }}</th>
                    <td>{{ note.ue.nom }}</td>
                    <td colspan=\"1\"><span class=\"badge badge-pill badge-style\">{{ note.lettre }}</span></td>
                    <td>
                        <form method=\"post\" onsubmit=\"deleteMessage()\" action={{ path('app_ue_delete', {id: note.id})}}>
                            <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
                            <input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token('delete' ~ note.id) }}\">
                            <button type=\"submit\" class=\"btn btn-danger btn-sm\"><i class=\"fas fa-trash\"></i></button>
                        </form>
                    </td>
                </tr>
                </tbody>
                {% else %}
                    <p>Aucune Ue suivie</p>
                {% endfor %}
{#                    {% for ue in user.ues %}#}
{#                    <tr>#}
{#                        <th scope=\"row\">{{ ue.code }}</th>#}
{#                        <td>{{ ue.nom }}</td>#}
{#                        <td colspan=\"1\"><span class=\"badge badge-pill badge-style\">A</span></td>#}
{#                    </tr>#}
{#                    </tbody>#}
{#                    {% else %}#}
{#                        <p>Aucune Ue suivie</p>#}
{#                    {% endfor %}#}
            </table>
            <div class=\"line\"></div>
            <div>
                <h6>Ajouter une UE suivie</h6>
                {% if form is defined %}
                {{ form_start(form) }}
                <div class=\"form-label-group\">
                    {{ form_widget(form) }}
                </div>
                <button class=\"btn btn-style-2\">{{ button_label|default('Ajouter') }}</button>
                {{ form_end(form) }}
                {% endif %}
            </div>
            <a id=\"generatePDF\"><button onclick=\"generatePDF()\" class=\"btn btn-style-2 down\">Télécharger mon profil en PDF</button></a>
        </div>
        <div class=\"au-card col-md-7 col-sm-12 profil\">
            <h5 class=\"float-right\">{{ user.firstname }} {{ user.lastname }}</h5>
            <br>
            <nav>
                <div class=\"nav nav-tabs\" id=\"chart\" role=\"tablist\">
                    <a class=\"nav-item nav-link active\" id=\"nav-chart-tab\" data-toggle=\"tab\" href=\"#nav-chart\" role=\"tab\" aria-controls=\"nav-chart\" aria-selected=\"true\">Profil</a>
                    <a class=\"nav-item nav-link\" id=\"nav-competence-tab\" data-toggle=\"tab\" href=\"#nav-competence\" role=\"tab\" aria-controls=\"nav-competence\" aria-selected=\"false\">Compétences</a>
               </div>
            </nav>

            <div class=\"tab-content\" id=\"nav-tabContent\">
                <div class=\"tab-pane fade show active\" id=\"nav-chart\" role=\"tabpanel\" aria-labelledby=\"nav-chart-tab\">
                    <div id=\"chartProfil\"></div>
                </div>
                <div class=\"tab-pane fade\" id=\"nav-competence\" role=\"tabpanel\" aria-labelledby=\"nav-competence-tab\">
                    <h6>Séléctionner la compétence que vous souhaitez consulter</h6>
                    <input class=\"inputTab\" id=\"tab1\" type=\"radio\" name=\"tabs\" checked>
                    <label class=\"competence\" for=\"tab1\"><span>Analyse</span></label>

                    <input class=\"inputTab\" id=\"tab2\" type=\"radio\" name=\"tabs\">
                    <label class=\"competence\" for=\"tab2\"><span>Conception</span></label>

                    <input class=\"inputTab\" id=\"tab3\" type=\"radio\" name=\"tabs\">
                    <label class=\"competence\" for=\"tab3\"><span>Réalisation</span></label>

                    <input class=\"inputTab\" id=\"tab4\" type=\"radio\" name=\"tabs\">
                    <label class=\"competence\" for=\"tab4\"><span>Déploiement</span></label>

                    <input class=\"inputTab\" id=\"tab5\" type=\"radio\" name=\"tabs\">
                    <label class=\"competence\" for=\"tab5\"><span>Gestion</span></label>

                    <section id=\"content1\" class=\"tab-content\">
                        <table id=\"table1\" class=\"table table-sm tableProfil\">
                            {% for microcompetence in microCompetences['analyse'] %}
                                <tr><td>{{ microcompetence.description }}</td></tr>
                            {% endfor %}
                        </table>
                    </section>

                    <section id=\"content2\" class=\"tab-content\">
                        <table id=\"table2\" class=\"table table-sm tableProfil\">
                            {% for microcompetence in microCompetences['conception'] %}
                                <tr><td>{{ microcompetence.description }}</td></tr>
                            {% endfor %}
                        </table>
                    </section>

                    <section id=\"content3\" class=\"tab-content\">
                        <table id=\"table3\" class=\"table table-sm tableProfil\">
                            {% for microcompetence in microCompetences['realisation'] %}
                                <tr><td>{{ microcompetence.description }}</td></tr>
                            {% endfor %}
                        </table>
                    </section>

                    <section id=\"content4\" class=\"tab-content\">
                        <table id=\"table4\" class=\"table table-sm tableProfil\">
                            {% for microcompetence in microCompetences['deploiement'] %}
                                <tr><td>{{ microcompetence.description }}</td></tr>
                            {% endfor %}
                        </table>
                    </section>

                    <section id=\"content5\" class=\"tab-content\">
                        <table id=\"table5\" class=\"table table-sm tableProfil\">
                            {% for microcompetence in microCompetences['gestion'] %}
                                <tr><td>{{ microcompetence.description }}</td></tr>
                            {% endfor %}
                        </table>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
      function generatePDF(){
          document.getElementById('src').value = document.getElementById('imgSrc').innerHTML;
          document.getElementById('imgData').submit();
      }
      function deleteMessage(){
          return confirm('Voulez vraiment suppprimer cet élément?');
      }
    </script>
{% endblock %}
", "profil.html.twig", "/var/www/tx_back/competences_isi/templates/profil.html.twig");
    }
}
