<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* sous_competence/_form.html.twig */
class __TwigTemplate_2f4b37a53aa7badbb1e3f996197724dc029f0964f92f0832eae4e2966015e66c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'otherscripts' => [$this, 'block_otherscripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "sous_competence/_form.html.twig"));

        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), 'form_start');
        echo "
<div class=\"row\">
    <div class=\"au-card col-md-5\">
        ";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 4, $this->source); })()), "description", [], "any", false, false, false, 4), 'row');
        echo "
        <div class=\"line\"></div>
        <div class=\"alert alert-warning\" role=\"alert\">
            Attention, une fois la compétence changée, les sous compétences seront décochées
        </div>
        ";
        // line 9
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 9, $this->source); })()), "competence", [], "any", false, false, false, 9), 'row');
        echo "
        <button class=\"btn btn-style-2\">";
        // line 10
        echo twig_escape_filter($this->env, (((isset($context["button_label"]) || array_key_exists("button_label", $context))) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 10, $this->source); })()), "Enregistrer")) : ("Enregistrer")), "html", null, true);
        echo "</button>
    </div>
    <div class=\"au-card col-md-6\">
        <div class=\"tableProfil\">
            <h5>Associées des sous compétences</h5>
            <div class=\"line\"></div>
        ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 16, $this->source); })()), "microcompetences", [], "any", false, false, false, 16), 'row');
        echo "
        </div>
    </div>
</div>
";
        // line 20
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 20, $this->source); })()), 'form_end');
        echo "

 ";
        // line 22
        $this->displayBlock('otherscripts', $context, $blocks);
        // line 67
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 22
    public function block_otherscripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "otherscripts"));

        // line 23
        echo "     <script>
         // Ajouter une classe du nom de la competence associée à tout les labels des checkbox sous compétences.
         let tab_comp = [\"analyse\", \"conception\",\"realisation\",\"deploiement\",\"gestion\"];
         for(let comp of tab_comp){
             for(let element of document.getElementsByClassName(comp)){
                 if(document.getElementById(element.id)){
                     document.getElementById(element.id).labels[0].className = comp;
                 }
             }
         }

         let elements  = \$(\".deploiement, .analyse, .conception, .realisation, .gestion\");

         // Cacher toutes les checkboxs
         for(let option of document.getElementsByClassName(\"analyse\")) option.hidden = true;
         for(let option of document.getElementsByClassName(\"conception\")) option.hidden = true;
         for(let option of document.getElementsByClassName(\"realisation\")) option.hidden = true;
         for(let option of document.getElementsByClassName(\"deploiement\")) option.hidden = true;
         for(let option of document.getElementsByClassName(\"gestion\")) option.hidden = true;


         var firstCheckboxCompetence = \$('input:checkbox:checked:first')[0];
         if(firstCheckboxCompetence !== undefined){
             var options = document.getElementsByClassName(firstCheckboxCompetence.classList[2]);
             for (let option of elements) option.hidden = true;
             for (let option of options) option.hidden = false;
             console.log(options[0])
             document.getElementById('competences').value = firstCheckboxCompetence.classList[2];
         }else{
             for(let option of document.getElementsByClassName(\"analyse\")) option.hidden = false;
         }


         // Changer les checkboxs si changement de compétence
         \$('#sous_competence_competence').on('change', function() {
             var selectCompetence = this.options[this.selectedIndex].innerHTML;
             var options = document.getElementsByClassName(selectCompetence);
             for (let option of elements) option.hidden = true;
             for (let option of options) option.hidden = false;
             // Déselecte toute le sous compétences si changements de compétences
             for (let checkbox of document.getElementsByTagName('input')) checkbox.checked = false;
         });
     </script>
 ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "sous_competence/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 23,  90 => 22,  82 => 67,  80 => 22,  75 => 20,  68 => 16,  59 => 10,  55 => 9,  47 => 4,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ form_start(form) }}
<div class=\"row\">
    <div class=\"au-card col-md-5\">
        {{ form_row(form.description) }}
        <div class=\"line\"></div>
        <div class=\"alert alert-warning\" role=\"alert\">
            Attention, une fois la compétence changée, les sous compétences seront décochées
        </div>
        {{ form_row(form.competence) }}
        <button class=\"btn btn-style-2\">{{ button_label|default('Enregistrer') }}</button>
    </div>
    <div class=\"au-card col-md-6\">
        <div class=\"tableProfil\">
            <h5>Associées des sous compétences</h5>
            <div class=\"line\"></div>
        {{ form_row(form.microcompetences) }}
        </div>
    </div>
</div>
{{ form_end(form) }}

 {% block otherscripts %}
     <script>
         // Ajouter une classe du nom de la competence associée à tout les labels des checkbox sous compétences.
         let tab_comp = [\"analyse\", \"conception\",\"realisation\",\"deploiement\",\"gestion\"];
         for(let comp of tab_comp){
             for(let element of document.getElementsByClassName(comp)){
                 if(document.getElementById(element.id)){
                     document.getElementById(element.id).labels[0].className = comp;
                 }
             }
         }

         let elements  = \$(\".deploiement, .analyse, .conception, .realisation, .gestion\");

         // Cacher toutes les checkboxs
         for(let option of document.getElementsByClassName(\"analyse\")) option.hidden = true;
         for(let option of document.getElementsByClassName(\"conception\")) option.hidden = true;
         for(let option of document.getElementsByClassName(\"realisation\")) option.hidden = true;
         for(let option of document.getElementsByClassName(\"deploiement\")) option.hidden = true;
         for(let option of document.getElementsByClassName(\"gestion\")) option.hidden = true;


         var firstCheckboxCompetence = \$('input:checkbox:checked:first')[0];
         if(firstCheckboxCompetence !== undefined){
             var options = document.getElementsByClassName(firstCheckboxCompetence.classList[2]);
             for (let option of elements) option.hidden = true;
             for (let option of options) option.hidden = false;
             console.log(options[0])
             document.getElementById('competences').value = firstCheckboxCompetence.classList[2];
         }else{
             for(let option of document.getElementsByClassName(\"analyse\")) option.hidden = false;
         }


         // Changer les checkboxs si changement de compétence
         \$('#sous_competence_competence').on('change', function() {
             var selectCompetence = this.options[this.selectedIndex].innerHTML;
             var options = document.getElementsByClassName(selectCompetence);
             for (let option of elements) option.hidden = true;
             for (let option of options) option.hidden = false;
             // Déselecte toute le sous compétences si changements de compétences
             for (let checkbox of document.getElementsByTagName('input')) checkbox.checked = false;
         });
     </script>
 {% endblock %}

", "sous_competence/_form.html.twig", "/var/www/tx_back/competences_isi/templates/sous_competence/_form.html.twig");
    }
}
