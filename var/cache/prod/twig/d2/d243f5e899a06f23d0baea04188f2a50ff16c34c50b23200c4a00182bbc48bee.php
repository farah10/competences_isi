<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_2e4265a3dcdd4219a159cfe47d145a5b78ab22a62b150a04cea961fbc2dc0d96 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta charset=\"UTF-8\">
        <script src=\"/public/js/googleChart.js\"></script>
        <script src=\"/public/js/jquery.js\"></script>

        <title>";
        // line 10
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        ";
        // line 12
        $this->displayBlock('head', $context, $blocks);
        // line 13
        echo "
        ";
        // line 14
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 21
        echo "
    </head>
    <body>
        <div class=\"wrapper\">
            <!-- Sidebar  -->
            <nav id=\"sidebar\">
                <div class=\"sidebar-header\">
                    <h3><i class=\"fas fa-laptop\"></i> Profil ISI</h3>
                    <strong>ISI</strong>
                </div>

                <ul class=\"list-unstyled components\">
                    <li>
                        <a href=";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_accueil");
        echo ">
                           <i class=\"fas fa-home\"></i>
                            Accueil
                        </a>
                    </li>
                    <li>
                        <a href=\"#catalogueSubmenu\" data-toggle=\"collapse\" aria-expanded=\"false\" class=\"dropdown-toggle\">
                            <i class=\"fas fa-copy\"></i>
                            Catalogue
                        </a>
                        <ul class=\"collapse list-unstyled\" id=\"catalogueSubmenu\">
                            <li>
                                <a href=";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_catalogue_ues");
        echo ">UE</a>
                            </li>
                            <li>
                                <a href=";
        // line 49
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_catalogue_competences");
        echo ">Compétences</a>
                            </li>
                        </ul>
                    </li>
                    ";
        // line 53
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
            // line 54
            echo "                        <li>
                            <a href=";
            // line 55
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_profil");
            echo ">
                                <i class=\"fas fa-graduation-cap\"></i>
                                Mon profil
                            </a>
                        </li>
                        <li>
                            <a href=";
            // line 61
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_choixUe");
            echo ">
                                <i class=\"fas fa-briefcase\"></i>
                                Choisir mes ues
                            </a>
                        </li>
                        <li>
                            <a href=";
            // line 67
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo ">
                                <i class=\"fas fa-sign-out-alt\"></i>
                                Se déconnecter
                            </a>
                        </li>
                    ";
        } elseif ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 73
            echo "                        <li>
                            <a href=";
            // line 74
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_admin");
            echo ">
                                <i class=\"fas fa-graduation-cap\"></i>
                                Administrer
                            </a>
                        </li>
                        <li>
                            <a href=";
            // line 80
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo ">
                                <i class=\"fas fa-sign-out-alt\"></i>
                                Se déconnecter
                            </a>
                        </li>
                    ";
        } else {
            // line 86
            echo "                        <li>
                            <a href=";
            // line 87
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_login");
            echo ">
                                <i class=\"fas fa-graduation-cap\"></i>
                                Se connecter
                            </a>
                        </li>
                        <li>
                            <a href=";
            // line 93
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_register");
            echo ">
                                <i class=\"fas fa-briefcase\"></i>
                                S'inscrire
                            </a>
                        </li>
                    ";
        }
        // line 99
        echo "                </ul>
            </nav>
            <div id=\"content\">
                <button type=\"button\" id=\"sidebarCollapse\" class=\"btn\">
                    <i class=\"fas fa-align-justify\"></i>
                </button>
                ";
        // line 105
        $this->displayBlock('body', $context, $blocks);
        // line 106
        echo "            </div>
        </div>

        ";
        // line 109
        $this->displayBlock('javascripts', $context, $blocks);
        // line 117
        echo "    </body>
</html>
";
    }

    // line 10
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Profil ISI";
    }

    // line 12
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 14
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "            <link rel=\"stylesheet\" href=\"/public/css/bootstrap.min.css\">
            <link rel=\"stylesheet\" href=\"/public/css/style.css\">
            <link rel=\"stylesheet\" href=\"/public/css/tab.css\">
            <link rel=\"stylesheet\" href=\"/public/css/accordion.css\">
            <link rel=\"stylesheet\" href=\"/public/css/form.css\">
        ";
    }

    // line 105
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 109
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 110
        echo "            <script src=\"/public/js/googleChart.js\"></script>
            <script src=\"/public/js/popper.min.js\"></script>
            <script src=\"/public/js/bootstrap.min.js\"></script>
            <script src=\"/public/js/main.js\"></script>
            <script defer src=\"https://use.fontawesome.com/releases/v5.0.13/js/solid.js\" integrity=\"sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ\" crossorigin=\"anonymous\"></script>
            <script defer src=\"https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js\" integrity=\"sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY\" crossorigin=\"anonymous\"></script>
        ";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  241 => 110,  237 => 109,  231 => 105,  222 => 15,  218 => 14,  212 => 12,  205 => 10,  199 => 117,  197 => 109,  192 => 106,  190 => 105,  182 => 99,  173 => 93,  164 => 87,  161 => 86,  152 => 80,  143 => 74,  140 => 73,  131 => 67,  122 => 61,  113 => 55,  110 => 54,  108 => 53,  101 => 49,  95 => 46,  80 => 34,  65 => 21,  63 => 14,  60 => 13,  58 => 12,  53 => 10,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base.html.twig", "/var/www/tx_back/competences_isi/templates/base.html.twig");
    }
}
