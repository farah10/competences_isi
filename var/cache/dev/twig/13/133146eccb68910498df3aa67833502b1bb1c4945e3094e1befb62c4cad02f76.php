<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* micro_competence/_form.html.twig */
class __TwigTemplate_4489d3cf07f5d5bd1284b51c430eeab2949a8b972df8aa9d8655b9e9872537b7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'otherscripts' => [$this, 'block_otherscripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "micro_competence/_form.html.twig"));

        // line 1
        echo "    <div class=\"row\">
        <div class=\"au-card col-md-3\">
                <label for=\"competences\">Compétence</label>
                <div class=\"alert alert-warning\" role=\"alert\">
                    Attention, une fois la compétence changée, les sous compétences et UEs associées seront décochées
                </div>
                <select id=\"competences\" class=\"form-control\">
                    <option value=\"analyse\">analyse</option>
                    <option value=\"conception\">conception</option>
                    <option value=\"realisation\">réalisation</option>
                    <option value=\"deploiement\">déploiement</option>
                    <option value=\"gestion\">gestion</option>
                </select>
            <div class=\"line\"></div>
            ";
        // line 15
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 15, $this->source); })()), 'form_start');
        echo "
            ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 16, $this->source); })()), "description", [], "any", false, false, false, 16), 'row');
        echo "
            <button id=\"save\" class=\"btn btn-style-2 float-left\">";
        // line 17
        echo twig_escape_filter($this->env, (((isset($context["button_label"]) || array_key_exists("button_label", $context))) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 17, $this->source); })()), "Enregistrer")) : ("Enregistrer")), "html", null, true);
        echo "</button>
        </div>
        <div class=\"au-card col-md-4\">
            <div class=\"tableProfilAdmin\">
                <h5>Choisir les sous-compétences associées</h5>
                <div class=\"line\"></div>
                ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 23, $this->source); })()), "sousCompetences", [], "any", false, false, false, 23), 'row');
        echo "
            </div>
        </div>
        <div class=\"au-card col-md-4\">
            <div class=\"tableProfilAdmin\">
                <h5>Choisir les UE associées</h5>
                <div class=\"line\"></div>
                ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 30, $this->source); })()), "ues", [], "any", false, false, false, 30), 'row');
        echo "
                ";
        // line 31
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 31, $this->source); })()), 'form_end');
        echo "
            </div>
        </div>
    </div>

    ";
        // line 36
        $this->displayBlock('otherscripts', $context, $blocks);
        // line 94
        echo "


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 36
    public function block_otherscripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "otherscripts"));

        // line 37
        echo "        <script>
            // Ajouter une classe du nom de la competence associée à tout les labels des checkbox sous compétences.
            let tab_comp = [\"analyse\", \"conception\",\"realisation\",\"deploiement\",\"gestion\"];
            for(let comp of tab_comp){
                for(let element of document.getElementsByClassName(comp)){
                    if(document.getElementById(element.id)){
                        document.getElementById(element.id).labels[0].className = comp;
                    }
                }
            }

            let saveButton = \$(\"#save\");
            let elements  = \$(\".deploiement, .analyse, .conception, .realisation, .gestion\");

            // Cacher toutes les checkboxs
            for(let option of document.getElementsByClassName(\"analyse\")) option.hidden = true;
            for(let option of document.getElementsByClassName(\"conception\")) option.hidden = true;
            for(let option of document.getElementsByClassName(\"realisation\")) option.hidden = true;
            for(let option of document.getElementsByClassName(\"deploiement\")) option.hidden = true;
            for(let option of document.getElementsByClassName(\"gestion\")) option.hidden = true;


            var firstCheckboxCompetence = \$('input:checkbox:checked:first')[0];
            if(firstCheckboxCompetence !== undefined){
                var options = document.getElementsByClassName(firstCheckboxCompetence.classList[2]);
                for (let option of elements) option.hidden = true;
                for (let option of options) option.hidden = false;
                console.log(options[0])
                document.getElementById('competences').value = firstCheckboxCompetence.classList[2];
                saveButton[0].disabled = false;
            }else{
                saveButton[0].disabled = true;
                for(let option of document.getElementsByClassName(\"analyse\")) option.hidden = false;
            }

            // Si aucune sous compétence n'est sélectionnée, bloquer le bouton d'envois du formulaire
            \$('#micro_competence_sousCompetences').on('change', function(){
                if(\$('input[name=\"micro_competence[sousCompetences][]\"]:checkbox:checked:first')[0] !== undefined){
                    document.getElementById('save').disabled = false;
                }else{
                    document.getElementById('save').disabled = true;
                }
            });

            // Changer les checkboxs si changement de compétence
            \$('#competences').on('change', function() {
                var selectCompetence = this.options[this.selectedIndex].value;
                var options = document.getElementsByClassName(selectCompetence);
                for (let option of elements) option.hidden = true;
                for (let option of options) option.hidden = false;
                \$('#sousCompetences').value = options[0].value
                // Déselecte toute le sous compétences si changements de compétences
                for (let checkbox of document.getElementsByTagName('input')) checkbox.checked = false;
                document.getElementById('save').disabled = true;
            });
        </script>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "micro_competence/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 37,  108 => 36,  98 => 94,  96 => 36,  88 => 31,  84 => 30,  74 => 23,  65 => 17,  61 => 16,  57 => 15,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("    <div class=\"row\">
        <div class=\"au-card col-md-3\">
                <label for=\"competences\">Compétence</label>
                <div class=\"alert alert-warning\" role=\"alert\">
                    Attention, une fois la compétence changée, les sous compétences et UEs associées seront décochées
                </div>
                <select id=\"competences\" class=\"form-control\">
                    <option value=\"analyse\">analyse</option>
                    <option value=\"conception\">conception</option>
                    <option value=\"realisation\">réalisation</option>
                    <option value=\"deploiement\">déploiement</option>
                    <option value=\"gestion\">gestion</option>
                </select>
            <div class=\"line\"></div>
            {{ form_start(form) }}
            {{ form_row(form.description) }}
            <button id=\"save\" class=\"btn btn-style-2 float-left\">{{ button_label|default('Enregistrer') }}</button>
        </div>
        <div class=\"au-card col-md-4\">
            <div class=\"tableProfilAdmin\">
                <h5>Choisir les sous-compétences associées</h5>
                <div class=\"line\"></div>
                {{ form_row(form.sousCompetences) }}
            </div>
        </div>
        <div class=\"au-card col-md-4\">
            <div class=\"tableProfilAdmin\">
                <h5>Choisir les UE associées</h5>
                <div class=\"line\"></div>
                {{ form_row(form.ues) }}
                {{ form_end(form) }}
            </div>
        </div>
    </div>

    {% block otherscripts %}
        <script>
            // Ajouter une classe du nom de la competence associée à tout les labels des checkbox sous compétences.
            let tab_comp = [\"analyse\", \"conception\",\"realisation\",\"deploiement\",\"gestion\"];
            for(let comp of tab_comp){
                for(let element of document.getElementsByClassName(comp)){
                    if(document.getElementById(element.id)){
                        document.getElementById(element.id).labels[0].className = comp;
                    }
                }
            }

            let saveButton = \$(\"#save\");
            let elements  = \$(\".deploiement, .analyse, .conception, .realisation, .gestion\");

            // Cacher toutes les checkboxs
            for(let option of document.getElementsByClassName(\"analyse\")) option.hidden = true;
            for(let option of document.getElementsByClassName(\"conception\")) option.hidden = true;
            for(let option of document.getElementsByClassName(\"realisation\")) option.hidden = true;
            for(let option of document.getElementsByClassName(\"deploiement\")) option.hidden = true;
            for(let option of document.getElementsByClassName(\"gestion\")) option.hidden = true;


            var firstCheckboxCompetence = \$('input:checkbox:checked:first')[0];
            if(firstCheckboxCompetence !== undefined){
                var options = document.getElementsByClassName(firstCheckboxCompetence.classList[2]);
                for (let option of elements) option.hidden = true;
                for (let option of options) option.hidden = false;
                console.log(options[0])
                document.getElementById('competences').value = firstCheckboxCompetence.classList[2];
                saveButton[0].disabled = false;
            }else{
                saveButton[0].disabled = true;
                for(let option of document.getElementsByClassName(\"analyse\")) option.hidden = false;
            }

            // Si aucune sous compétence n'est sélectionnée, bloquer le bouton d'envois du formulaire
            \$('#micro_competence_sousCompetences').on('change', function(){
                if(\$('input[name=\"micro_competence[sousCompetences][]\"]:checkbox:checked:first')[0] !== undefined){
                    document.getElementById('save').disabled = false;
                }else{
                    document.getElementById('save').disabled = true;
                }
            });

            // Changer les checkboxs si changement de compétence
            \$('#competences').on('change', function() {
                var selectCompetence = this.options[this.selectedIndex].value;
                var options = document.getElementsByClassName(selectCompetence);
                for (let option of elements) option.hidden = true;
                for (let option of options) option.hidden = false;
                \$('#sousCompetences').value = options[0].value
                // Déselecte toute le sous compétences si changements de compétences
                for (let checkbox of document.getElementsByTagName('input')) checkbox.checked = false;
                document.getElementById('save').disabled = true;
            });
        </script>
    {% endblock %}



", "micro_competence/_form.html.twig", "/var/www/tx_back/competences_isi/templates/micro_competence/_form.html.twig");
    }
}
