<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompetenceRepository")
 */
class Competence
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $completeName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SousCompetence", mappedBy="competence")
     */
    private $sousCompetences;

    public function __construct()
    {
        $this->sousCompetences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCompleteName(): ?string
    {
        return $this->completeName;
    }

    public function setCompleteName(string $completeName): self
    {
        $this->completeName = $completeName;

        return $this;
    }

    /**
     * @return Collection|SousCompetence[]
     */
    public function getSousCompetences(): Collection
    {
        return $this->sousCompetences;
    }

    public function addSousCompetence(SousCompetence $sousCompetence): self
    {
        if (!$this->sousCompetences->contains($sousCompetence)) {
            $this->sousCompetences[] = $sousCompetence;
            $sousCompetence->setCompetence($this);
        }

        return $this;
    }

    public function removeSousCompetence(SousCompetence $sousCompetence): self
    {
        if ($this->sousCompetences->contains($sousCompetence)) {
            $this->sousCompetences->removeElement($sousCompetence);
            // set the owning side to null (unless already changed)
            if ($sousCompetence->getCompetence() === $this) {
                $sousCompetence->setCompetence(null);
            }
        }

        return $this;
    }
}
