<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* sous_competence/adminSousCompetence.html.twig */
class __TwigTemplate_a63a9bc652c6b3fa944f39f490a8984621a2879a3c989cfa504205d687b46cca extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'otherscripts' => [$this, 'block_otherscripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "sous_competence/adminSousCompetence.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "sous_competence/adminSousCompetence.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Modifier des sous compétences";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"tab_container\">
    <h1>Modifier des sous compétences</h1>
    <hr class=\"my-4\">

    <div class=\"row\">
        <div class=\"au-card col-md-12\">
            <h6>Recherche de sous compétences par compétence</h6>
            <div class=\"line\"></div>
            <select id=\"competences\" class=\"form-control\">
                <option value=\"analyse\">analyse</option>
                <option value=\"conception\">conception</option>
                <option value=\"realisation\">réalisation</option>
                <option value=\"deploiement\">déploiement</option>
                <option value=\"gestion\">gestion</option>
            </select>
            <div class=\"line\"></div>
            <table class=\"table table-sm\">
                <thead>
                <tr>
                    <th scope=\"col\">#</th>
                    <th scope=\"col\">Sous Compétence</th>
                    <th scope=\"col\">Editer</th>
                </tr>
                </thead>
                ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sousCompetences"]) || array_key_exists("sousCompetences", $context) ? $context["sousCompetences"] : (function () { throw new RuntimeError('Variable "sousCompetences" does not exist.', 30, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["sousCompetence"]) {
            // line 31
            echo "                    <tbody>
                    <tr class= ";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["sousCompetence"], "competence", [], "any", false, false, false, 32), "name", [], "any", false, false, false, 32), "html", null, true);
            echo ">
                        <th scope=\"row\">";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["sousCompetence"], "id", [], "any", false, false, false, 33), "html", null, true);
            echo "</th>
                        <td>";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["sousCompetence"], "description", [], "any", false, false, false, 34), "html", null, true);
            echo "</td>
                        <td colspan=\"1\">
                            <a href=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sous_competence_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["sousCompetence"], "id", [], "any", false, false, false, 36)]), "html", null, true);
            echo "\">
                                <button class=\"btn btn-sm btn-secondary\"><i class=\"fas fa-edit\"></i></button>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 43
            echo "                    <p>Aucune Ue trouvée</p>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sousCompetence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "            </table>
            <a href=\"";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sous_competence_new");
        echo "\">
                <button class=\"btn btn-sm btn-secondary float-right\">
                    <i class=\"fas fa-plus\"></i>
                    Nouvelle micro compétence
                </button>
            </a>
        </div>
    </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 58
    public function block_otherscripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "otherscripts"));

        // line 59
        echo "    <script>
        // Afficher uniquement les options pour la compétence analyse dans un premier temps
        for(let option of document.getElementsByClassName(\"conception\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"realisation\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"deploiement\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"gestion\")) option.hidden = true;

        // Changer les options si changement de compétence
        \$('#competences').on('change', function() {
            var selectCompetence = this.options[this.selectedIndex].value;
            console.log(selectCompetence);
            var options = document.getElementsByClassName(selectCompetence);
            var elements  = \$(\".deploiement, .analyse, .conception, .realisation, .gestion\");
            for (let option of elements) option.hidden = true;
            for (let option of options) option.hidden = false;
            for (let checkbox of document.getElementsByTagName('input')) checkbox.checked = false;
            document.getElementById('admin_micro_competence_sousCompetence').value = options[0].value;
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "sous_competence/adminSousCompetence.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 59,  161 => 58,  143 => 46,  140 => 45,  133 => 43,  121 => 36,  116 => 34,  112 => 33,  108 => 32,  105 => 31,  100 => 30,  74 => 6,  67 => 5,  54 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Modifier des sous compétences{% endblock %}

{% block body %}
<div class=\"tab_container\">
    <h1>Modifier des sous compétences</h1>
    <hr class=\"my-4\">

    <div class=\"row\">
        <div class=\"au-card col-md-12\">
            <h6>Recherche de sous compétences par compétence</h6>
            <div class=\"line\"></div>
            <select id=\"competences\" class=\"form-control\">
                <option value=\"analyse\">analyse</option>
                <option value=\"conception\">conception</option>
                <option value=\"realisation\">réalisation</option>
                <option value=\"deploiement\">déploiement</option>
                <option value=\"gestion\">gestion</option>
            </select>
            <div class=\"line\"></div>
            <table class=\"table table-sm\">
                <thead>
                <tr>
                    <th scope=\"col\">#</th>
                    <th scope=\"col\">Sous Compétence</th>
                    <th scope=\"col\">Editer</th>
                </tr>
                </thead>
                {% for sousCompetence in sousCompetences %}
                    <tbody>
                    <tr class= {{ sousCompetence.competence.name }}>
                        <th scope=\"row\">{{ sousCompetence.id }}</th>
                        <td>{{ sousCompetence.description }}</td>
                        <td colspan=\"1\">
                            <a href=\"{{ path('sous_competence_edit', {id: sousCompetence.id})}}\">
                                <button class=\"btn btn-sm btn-secondary\"><i class=\"fas fa-edit\"></i></button>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                {% else %}
                    <p>Aucune Ue trouvée</p>
                {% endfor %}
            </table>
            <a href=\"{{ path('sous_competence_new')}}\">
                <button class=\"btn btn-sm btn-secondary float-right\">
                    <i class=\"fas fa-plus\"></i>
                    Nouvelle micro compétence
                </button>
            </a>
        </div>
    </div>
</div>

{% endblock %}

{% block otherscripts %}
    <script>
        // Afficher uniquement les options pour la compétence analyse dans un premier temps
        for(let option of document.getElementsByClassName(\"conception\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"realisation\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"deploiement\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"gestion\")) option.hidden = true;

        // Changer les options si changement de compétence
        \$('#competences').on('change', function() {
            var selectCompetence = this.options[this.selectedIndex].value;
            console.log(selectCompetence);
            var options = document.getElementsByClassName(selectCompetence);
            var elements  = \$(\".deploiement, .analyse, .conception, .realisation, .gestion\");
            for (let option of elements) option.hidden = true;
            for (let option of options) option.hidden = false;
            for (let checkbox of document.getElementsByTagName('input')) checkbox.checked = false;
            document.getElementById('admin_micro_competence_sousCompetence').value = options[0].value;
        });
    </script>
{% endblock %}
", "sous_competence/adminSousCompetence.html.twig", "/var/www/tx_back/competences_isi/templates/sous_competence/adminSousCompetence.html.twig");
    }
}
