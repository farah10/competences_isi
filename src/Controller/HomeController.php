<?php
// src/Controller/HomeController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class HomeController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/", name="app_accueil")
     */

    public function index()
    {
        if($this->security->isGranted('ROLE_USER')) {
            return $this->forward('App\Controller\ProfilController::index');
        }elseif ($this->security->isGranted('ROLE_ADMIN')){
            return $this->forward('App\Controller\AdminController::index');
        }else{
            return $this->render('index.html.twig');
        }
    }
}
