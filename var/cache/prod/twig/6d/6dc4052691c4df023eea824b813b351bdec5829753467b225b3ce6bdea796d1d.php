<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_6f92b2ea18af159dc2347953979ef341a148361e88e4b03e417dd399376f97ff extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Bienvenue sur Profil ISI";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "<div class=\"tab_container\">
    <h1>Bienvenue sur Profil ISI</h1>
    <h5>Construisez et personalisez votre parcours ISI</h5>
    <hr class=\"my-4\">


    <div class=\"row\">
        <div class=\"au-card col-12\">
            <h6>Cette plateforme vous accompagnera lors de votre cursus pour :</h6>
            <br>
            <ul>
                <li>Choisir les compétences que vous souhaitez développer</li>
                <li>Choisir vos UEs en conséquences</li>
                <li>Visualiser vos UEs et votre profil de compétences</li>
                <li>Partager facilement votre profil</li>
            </ul>
        </div>

    </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "index.html.twig", "/var/www/tx_back/competences_isi/templates/index.html.twig");
    }
}
