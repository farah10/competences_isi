<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* pdf.html.twig */
class __TwigTemplate_149e78f7bf2d2f3dfc726e15b9e7536fa1a19e1504d9965ffc719f0f36c9e494 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "pdf.html.twig"));

        // line 3
        $this->loadTemplate("style.html", "pdf.html.twig", 3)->display($context);
        // line 4
        echo "
<div class=\"row\">
    <div class=\"au-card col-sm-12\">
        <p>Profil de compétences de ";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 7, $this->source); })()), "firstname", [], "any", false, false, false, 7), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 7, $this->source); })()), "lastname", [], "any", false, false, false, 7), "html", null, true);
        echo "</p>
        ";
        // line 9
        echo "        <hr class=\"my-4\">
    </div>

</div>

    <div class=\"row\">
        <div class=\"au-card col-sm-12\">
            <img src=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["srcChart"]) || array_key_exists("srcChart", $context) ? $context["srcChart"] : (function () { throw new RuntimeError('Variable "srcChart" does not exist.', 16, $this->source); })()), "html", null, true);
        echo "\">
        </div>
    </div>

    <div class=\"row\">
        <div class=\"au-card col-sm-12\">
            <table class=\"table table-sm\">
                <thead>
                <tr>
                    <th scope=\"col\">#</th>
                    <th scope=\"col\">Cours suivis</th>
                    <th>Lettre obtenue</th>
                </tr>
                </thead>
                <tbody>
                ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["notes"]) || array_key_exists("notes", $context) ? $context["notes"] : (function () { throw new RuntimeError('Variable "notes" does not exist.', 31, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["note"]) {
            // line 32
            echo "                <tr>
                    <th scope=\"row\">";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["note"], "ue", [], "any", false, false, false, 33), "code", [], "any", false, false, false, 33), "html", null, true);
            echo "</th>
                    <td>";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["note"], "ue", [], "any", false, false, false, 34), "nom", [], "any", false, false, false, 34), "html", null, true);
            echo "</td>
                    <td>";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["note"], "lettre", [], "any", false, false, false, 35), "html", null, true);
            echo "</td>
                </tr>
                </tbody>
                ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 39
            echo "                    <p>Aucun cours suivie</p>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['note'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "            </table>
        </div>
    </div>

    <div class=\"line\"></div>

    ";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["microCompetences"]) || array_key_exists("microCompetences", $context) ? $context["microCompetences"] : (function () { throw new RuntimeError('Variable "microCompetences" does not exist.', 47, $this->source); })()));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 48
            echo "        <div class=\"row\">
            <div class=\"au-card col-sm-12\">
                <table class=\"table table-sm\">
                    <thead class=\"";
            // line 51
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">
                    <tr>
                        <th scope=\"col\">Micro Compétences acquises en ";
            // line 53
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo " </th>
                    </tr>
                    </thead>
                    <tbody>
                        ";
            // line 57
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["value"]);
            foreach ($context['_seq'] as $context["_key"] => $context["microCompetence"]) {
                // line 58
                echo "                            <tr>
                                <td scope=\"col\">";
                // line 59
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["microCompetence"], "description", [], "any", false, false, false, 59), "html", null, true);
                echo "</td>
                            </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['microCompetence'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 62
            echo "                    </tbody>
                </table>
            </div>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "pdf.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 67,  157 => 62,  148 => 59,  145 => 58,  141 => 57,  134 => 53,  129 => 51,  124 => 48,  120 => 47,  112 => 41,  105 => 39,  96 => 35,  92 => 34,  88 => 33,  85 => 32,  80 => 31,  62 => 16,  53 => 9,  47 => 7,  42 => 4,  40 => 3,);
    }

    public function getSourceContext()
    {
        return new Source("{# Ne peut pas générer le css dans le pdf autrement que sous cette forme #}
{# TODO trouver une meilleure solution pour intégrer le bootstrap dans le pdf ou faire un css propre au pdf #}
{% include 'style.html'%}

<div class=\"row\">
    <div class=\"au-card col-sm-12\">
        <p>Profil de compétences de {{ user.firstname }} {{ user.lastname }}</p>
        {# TODO Ajouter la fillière de l'étudiant si elle existe #}
        <hr class=\"my-4\">
    </div>

</div>

    <div class=\"row\">
        <div class=\"au-card col-sm-12\">
            <img src=\"{{ srcChart }}\">
        </div>
    </div>

    <div class=\"row\">
        <div class=\"au-card col-sm-12\">
            <table class=\"table table-sm\">
                <thead>
                <tr>
                    <th scope=\"col\">#</th>
                    <th scope=\"col\">Cours suivis</th>
                    <th>Lettre obtenue</th>
                </tr>
                </thead>
                <tbody>
                {% for note in notes %}
                <tr>
                    <th scope=\"row\">{{ note.ue.code }}</th>
                    <td>{{ note.ue.nom }}</td>
                    <td>{{ note.lettre }}</td>
                </tr>
                </tbody>
                {% else %}
                    <p>Aucun cours suivie</p>
                {% endfor %}
            </table>
        </div>
    </div>

    <div class=\"line\"></div>

    {% for key,value in microCompetences %}
        <div class=\"row\">
            <div class=\"au-card col-sm-12\">
                <table class=\"table table-sm\">
                    <thead class=\"{{ key }}\">
                    <tr>
                        <th scope=\"col\">Micro Compétences acquises en {{ key }} </th>
                    </tr>
                    </thead>
                    <tbody>
                        {% for microCompetence in value %}
                            <tr>
                                <td scope=\"col\">{{ microCompetence.description }}</td>
                            </tr>
                        {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
    {% endfor %}



", "pdf.html.twig", "/var/www/tx_back/competences_isi/templates/pdf.html.twig");
    }
}
