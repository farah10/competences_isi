<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200510074701 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sous_competence_micro_competence (sous_competence_id INT NOT NULL, micro_competence_id INT NOT NULL, INDEX IDX_8460B39628111824 (sous_competence_id), INDEX IDX_8460B3969CF52B1D (micro_competence_id), PRIMARY KEY(sous_competence_id, micro_competence_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sous_competence_micro_competence ADD CONSTRAINT FK_8460B39628111824 FOREIGN KEY (sous_competence_id) REFERENCES sous_competence (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sous_competence_micro_competence ADD CONSTRAINT FK_8460B3969CF52B1D FOREIGN KEY (micro_competence_id) REFERENCES micro_competence (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE micro_competence DROP FOREIGN KEY FK_4366FA8B1AE938F');
        $this->addSql('DROP INDEX IDX_4366FA8B1AE938F ON micro_competence');
        $this->addSql('ALTER TABLE micro_competence DROP sous_competence_id_id');
        $this->addSql('ALTER TABLE resultat CHANGE lettre lettre VARCHAR(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE sous_competence DROP FOREIGN KEY FK_72AC3D95F566CFB1');
        $this->addSql('DROP INDEX IDX_72AC3D95F566CFB1 ON sous_competence');
        $this->addSql('ALTER TABLE sous_competence CHANGE competence_id_id competence_id INT NOT NULL');
        $this->addSql('ALTER TABLE sous_competence ADD CONSTRAINT FK_72AC3D9515761DAB FOREIGN KEY (competence_id) REFERENCES competence (id)');
        $this->addSql('CREATE INDEX IDX_72AC3D9515761DAB ON sous_competence (competence_id)');
        $this->addSql('ALTER TABLE ue CHANGE semestre semestre VARCHAR(255) DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE sous_competence_micro_competence');
        $this->addSql('ALTER TABLE micro_competence ADD sous_competence_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE micro_competence ADD CONSTRAINT FK_4366FA8B1AE938F FOREIGN KEY (sous_competence_id_id) REFERENCES sous_competence (id)');
        $this->addSql('CREATE INDEX IDX_4366FA8B1AE938F ON micro_competence (sous_competence_id_id)');
        $this->addSql('ALTER TABLE resultat CHANGE lettre lettre VARCHAR(1) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE sous_competence DROP FOREIGN KEY FK_72AC3D9515761DAB');
        $this->addSql('DROP INDEX IDX_72AC3D9515761DAB ON sous_competence');
        $this->addSql('ALTER TABLE sous_competence CHANGE competence_id competence_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE sous_competence ADD CONSTRAINT FK_72AC3D95F566CFB1 FOREIGN KEY (competence_id_id) REFERENCES competence (id)');
        $this->addSql('CREATE INDEX IDX_72AC3D95F566CFB1 ON sous_competence (competence_id_id)');
        $this->addSql('ALTER TABLE ue CHANGE semestre semestre VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE type type VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin');
    }
}
