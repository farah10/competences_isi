<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ue/adminUe.html.twig */
class __TwigTemplate_66643591000f5c53e85ba0b1928ac085a402a8eb0c7c5a7fdd7bf737ef34f95a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ue/adminUe.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "ue/adminUe.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Modifier des Ues";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"tab_container\">
    <h1>Modifier des Ues</h1>
    <hr class=\"my-4\">
    <div class=\"row\">
        <div class=\"au-card col-md-7\">
            <table class=\"table tableProfilAdmin table-sm\">
                    <thead>
                    <tr>
                        <th scope=\"col\">#</th>
                        <th scope=\"col\">UE</th>
                        <th scope=\"col\">Editer</th>
                    </tr>
                    </thead>
                    ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["ues"]) || array_key_exists("ues", $context) ? $context["ues"] : (function () { throw new RuntimeError('Variable "ues" does not exist.', 19, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["ue"]) {
            // line 20
            echo "                        <tbody>
                        <tr>
                            <th scope=\"row\">";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ue"], "code", [], "any", false, false, false, 22), "html", null, true);
            echo "</th>
                            <td>";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ue"], "nom", [], "any", false, false, false, 23), "html", null, true);
            echo "</td>
                            <td colspan=\"1\">
                                <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ue_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["ue"], "id", [], "any", false, false, false, 25)]), "html", null, true);
            echo "\">
                                    <button class=\"btn btn-sm btn-secondary\"><i class=\"fas fa-edit\"></i></button>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 32
            echo "                        <p>Aucune Ue trouvée</p>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "            </table>
            <a href=\"";
        // line 35
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ue_new");
        echo "\">
                <button class=\"btn btn-style-2 float-lg-right\"> <i class=\"fas fa-plus\"></i> Ajouter une UE</button>
            </a>
        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ue/adminUe.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 35,  125 => 34,  118 => 32,  106 => 25,  101 => 23,  97 => 22,  93 => 20,  88 => 19,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Modifier des Ues{% endblock %}

{% block body %}
<div class=\"tab_container\">
    <h1>Modifier des Ues</h1>
    <hr class=\"my-4\">
    <div class=\"row\">
        <div class=\"au-card col-md-7\">
            <table class=\"table tableProfilAdmin table-sm\">
                    <thead>
                    <tr>
                        <th scope=\"col\">#</th>
                        <th scope=\"col\">UE</th>
                        <th scope=\"col\">Editer</th>
                    </tr>
                    </thead>
                    {% for ue in ues %}
                        <tbody>
                        <tr>
                            <th scope=\"row\">{{ ue.code }}</th>
                            <td>{{ ue.nom }}</td>
                            <td colspan=\"1\">
                                <a href=\"{{ path('ue_edit', {id: ue.id})}}\">
                                    <button class=\"btn btn-sm btn-secondary\"><i class=\"fas fa-edit\"></i></button>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    {% else %}
                        <p>Aucune Ue trouvée</p>
                    {% endfor %}
            </table>
            <a href=\"{{ path('ue_new') }}\">
                <button class=\"btn btn-style-2 float-lg-right\"> <i class=\"fas fa-plus\"></i> Ajouter une UE</button>
            </a>
        </div>
    </div>
</div>
{% endblock %}
", "ue/adminUe.html.twig", "/var/www/tx_back/competences_isi/templates/ue/adminUe.html.twig");
    }
}
