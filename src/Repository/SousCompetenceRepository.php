<?php

namespace App\Repository;

use App\Entity\SousCompetence;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SousCompetence|null find($id, $lockMode = null, $lockVersion = null)
 * @method SousCompetence|null findOneBy(array $criteria, array $orderBy = null)
 * @method SousCompetence[]    findAll()
 * @method SousCompetence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SousCompetenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SousCompetence::class);
    }

    // /**
    //  * @return SousCompetence[] Returns an array of SousCompetence objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SousCompetence
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
