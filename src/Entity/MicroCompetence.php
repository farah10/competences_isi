<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MicroCompetenceRepository")
 */
class MicroCompetence
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SousCompetence", mappedBy="microCompetences")
     */
    private $sousCompetences;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Ue", mappedBy="microCompetences")
     */
    private $ues;

    public function __construct()
    {
        $this->ues = new ArrayCollection();
        $this->sousCompetences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|SousCompetence[]
     */
    public function getSousCompetences(): Collection
    {
        return $this->sousCompetences;
    }

    public function addSousCompetence(SousCompetence $sousCompetence): self
    {
        if (!$this->sousCompetences->contains($sousCompetence)) {
            $this->sousCompetences[] = $sousCompetence;
            $sousCompetence->addMicroCompetence($this);
        }

        return $this;
    }

    public function removeSousCompetence(SousCompetence $sousCompetence): self
    {
        if ($this->sousCompetences->contains($sousCompetence)) {
            $this->sousCompetences->removeElement($sousCompetence);
            $sousCompetence->removeMicroCompetence($this);
        }

        return $this;
    }


    /**
     * @return Collection|Ue[]
     */
    public function getUes(): Collection
    {
        return $this->ues;
    }

    public function addUe(Ue $ue): self
    {
        if (!$this->ues->contains($ue)) {
            $this->ues[] = $ue;
            $ue->addMicroCompetence($this);
        }

        return $this;
    }

    public function removeUe(Ue $ue): self
    {
        if ($this->ues->contains($ue)) {
            $this->ues->removeElement($ue);
            $ue->removeMicroCompetence($this);
        }

        return $this;
    }
}
