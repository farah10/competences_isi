<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ue/_form.html.twig */
class __TwigTemplate_82df380681931ce0c1dc5ded5ec0bdcef97efe61524e9648e3cd02c922f68aaf extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'otherscripts' => [$this, 'block_otherscripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ue/_form.html.twig"));

        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), 'form_start');
        echo "
    <div class=\"row\">
            <div class=\"au-card col-md-5\">
                ";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 4, $this->source); })()), "code", [], "any", false, false, false, 4), 'row');
        echo "
                ";
        // line 5
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 5, $this->source); })()), "nom", [], "any", false, false, false, 5), 'row');
        echo "
                ";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 6, $this->source); })()), "description", [], "any", false, false, false, 6), 'row');
        echo "
                ";
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 7, $this->source); })()), "programme", [], "any", false, false, false, 7), 'row');
        echo "
                ";
        // line 8
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 8, $this->source); })()), "semestre", [], "any", false, false, false, 8), 'row');
        echo "
                ";
        // line 9
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 9, $this->source); })()), "type", [], "any", false, false, false, 9), 'row');
        echo "
            </div>
            <div class=\"au-card col-md-6\">
                <div class=\"tableProfilAdmin\">
                    <label>Choisir une compétence pour voir les micro compétences correspondantes</label>
                    <select id=\"competences\" class=\"form-control\">
                        <option value=\"analyse\">analyse</option>
                        <option value=\"conception\">conception</option>
                        <option value=\"realisation\">réalisation</option>
                        <option value=\"deploiement\">déploiement</option>
                        <option value=\"gestion\">gestion</option>
                    </select>
                    <div class=\"line\"></div>
                    <h6>Micro compétences associées</h6>
                    <div class=\"line\"></div>
                    ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 24, $this->source); })()), "microcompetences", [], "any", false, false, false, 24), 'widget');
        echo "
                </div>
                <button id=\"save\" class=\"btn btn-style-2 down\">";
        // line 26
        echo twig_escape_filter($this->env, (((isset($context["button_label"]) || array_key_exists("button_label", $context))) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 26, $this->source); })()), "Enregistrer")) : ("Enregistrer")), "html", null, true);
        echo "</button>
            </div>
    </div>
";
        // line 29
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 29, $this->source); })()), 'form_end');
        echo "
";
        // line 30
        $this->displayBlock('otherscripts', $context, $blocks);
        // line 73
        echo "

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 30
    public function block_otherscripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "otherscripts"));

        // line 31
        echo "    <script>
        // Ajouter une classe du nom de la competence associée à tout les labels des checkbox sous compétences.
        let tab_comp = [\"analyse\", \"conception\",\"realisation\",\"deploiement\",\"gestion\"];
        for(let comp of tab_comp){
            for(let element of document.getElementsByClassName(comp)){
                if(document.getElementById(element.id)){
                    document.getElementById(element.id).labels[0].className = comp;
                }
            }
        }

        let elements  = \$(\".deploiement, .analyse, .conception, .realisation, .gestion\");

        // Cacher toutes les checkboxs
        for(let option of document.getElementsByClassName(\"analyse\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"conception\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"realisation\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"deploiement\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"gestion\")) option.hidden = true;


        var firstCheckboxCompetence = \$('input:checkbox:checked:first')[0];
        if(firstCheckboxCompetence !== undefined){
            var options = document.getElementsByClassName(firstCheckboxCompetence.classList[2]);
            for (let option of elements) option.hidden = true;
            for (let option of options) option.hidden = false;
            console.log(options[0])
            document.getElementById('competences').value = firstCheckboxCompetence.classList[2];
        }else{
            for(let option of document.getElementsByClassName(\"analyse\")) option.hidden = false;
        }

        // Changer les checkboxs si changement de compétence
        \$('#competences').on('change', function() {
            var selectCompetence = this.options[this.selectedIndex].value;
            var options = document.getElementsByClassName(selectCompetence);
            for (let option of elements) option.hidden = true;
            for (let option of options) option.hidden = false;
            \$('#sousCompetences').value = options[0].value
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ue/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 31,  111 => 30,  102 => 73,  100 => 30,  96 => 29,  90 => 26,  85 => 24,  67 => 9,  63 => 8,  59 => 7,  55 => 6,  51 => 5,  47 => 4,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ form_start(form) }}
    <div class=\"row\">
            <div class=\"au-card col-md-5\">
                {{ form_row(form.code) }}
                {{ form_row(form.nom) }}
                {{ form_row(form.description) }}
                {{ form_row(form.programme) }}
                {{ form_row(form.semestre) }}
                {{ form_row(form.type) }}
            </div>
            <div class=\"au-card col-md-6\">
                <div class=\"tableProfilAdmin\">
                    <label>Choisir une compétence pour voir les micro compétences correspondantes</label>
                    <select id=\"competences\" class=\"form-control\">
                        <option value=\"analyse\">analyse</option>
                        <option value=\"conception\">conception</option>
                        <option value=\"realisation\">réalisation</option>
                        <option value=\"deploiement\">déploiement</option>
                        <option value=\"gestion\">gestion</option>
                    </select>
                    <div class=\"line\"></div>
                    <h6>Micro compétences associées</h6>
                    <div class=\"line\"></div>
                    {{ form_widget(form.microcompetences) }}
                </div>
                <button id=\"save\" class=\"btn btn-style-2 down\">{{ button_label|default('Enregistrer') }}</button>
            </div>
    </div>
{{ form_end(form) }}
{% block otherscripts %}
    <script>
        // Ajouter une classe du nom de la competence associée à tout les labels des checkbox sous compétences.
        let tab_comp = [\"analyse\", \"conception\",\"realisation\",\"deploiement\",\"gestion\"];
        for(let comp of tab_comp){
            for(let element of document.getElementsByClassName(comp)){
                if(document.getElementById(element.id)){
                    document.getElementById(element.id).labels[0].className = comp;
                }
            }
        }

        let elements  = \$(\".deploiement, .analyse, .conception, .realisation, .gestion\");

        // Cacher toutes les checkboxs
        for(let option of document.getElementsByClassName(\"analyse\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"conception\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"realisation\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"deploiement\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"gestion\")) option.hidden = true;


        var firstCheckboxCompetence = \$('input:checkbox:checked:first')[0];
        if(firstCheckboxCompetence !== undefined){
            var options = document.getElementsByClassName(firstCheckboxCompetence.classList[2]);
            for (let option of elements) option.hidden = true;
            for (let option of options) option.hidden = false;
            console.log(options[0])
            document.getElementById('competences').value = firstCheckboxCompetence.classList[2];
        }else{
            for(let option of document.getElementsByClassName(\"analyse\")) option.hidden = false;
        }

        // Changer les checkboxs si changement de compétence
        \$('#competences').on('change', function() {
            var selectCompetence = this.options[this.selectedIndex].value;
            var options = document.getElementsByClassName(selectCompetence);
            for (let option of elements) option.hidden = true;
            for (let option of options) option.hidden = false;
            \$('#sousCompetences').value = options[0].value
        });
    </script>
{% endblock %}


", "ue/_form.html.twig", "/var/www/tx_back/competences_isi/templates/ue/_form.html.twig");
    }
}
