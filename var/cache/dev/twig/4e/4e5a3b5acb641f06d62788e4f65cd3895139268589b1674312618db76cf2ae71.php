<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* micro_competence/adminMicroCompetence.html.twig */
class __TwigTemplate_018d459b1c68f35a639a49ccced5fcbb7f62212febd24485e0e17273be41773e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'otherscripts' => [$this, 'block_otherscripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "micro_competence/adminMicroCompetence.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "micro_competence/adminMicroCompetence.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Editer des micro compétences";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "<div class=\"tab_container\">
    <h1>Modifier des micro compétences</h1>
    <hr class=\"my-4\">

    <div class=\"row\">
        <div class=\"au-card col-md-5\">
            <h5>Chercher une micro compétence à éditer</h5>
            <div class=\"line\"></div>
            ";
        // line 15
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 15, $this->source); })()), 'form_start');
        echo "
                ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 16, $this->source); })()), 'widget');
        echo "
                <button class=\"btn btn-style-2\">";
        // line 17
        echo twig_escape_filter($this->env, (((isset($context["button_label"]) || array_key_exists("button_label", $context))) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 17, $this->source); })()), "Rechercher les microcompétences")) : ("Rechercher les microcompétences")), "html", null, true);
        echo "</button>
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 18, $this->source); })()), 'form_end');
        echo "
            <div class=\"line\"></div>
            <a href=\"";
        // line 20
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("micro_competence_new");
        echo "\">
                <button class=\"btn btn-sm btn-secondary float-right\">
                    <i class=\"fas fa-plus\"></i>
                    Nouvelle micro compétence
                </button>
            </a>
        </div>
        ";
        // line 27
        if (((isset($context["microcompetences"]) || array_key_exists("microcompetences", $context)) &&  !twig_test_empty((isset($context["microcompetences"]) || array_key_exists("microcompetences", $context) ? $context["microcompetences"] : (function () { throw new RuntimeError('Variable "microcompetences" does not exist.', 27, $this->source); })())))) {
            // line 28
            echo "            <div class=\"au-card col-md-6\">
                <table class=\"table table-hover tableProfil tableStyle\">
                    <thead>
                        <th scope=\"row\">Id</th>
                        <th>Microcompétence</th>
                        <th>Editer</th>
                    </thead>
                    <tbody>
                        ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["microcompetences"]) || array_key_exists("microcompetences", $context) ? $context["microcompetences"] : (function () { throw new RuntimeError('Variable "microcompetences" does not exist.', 36, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["microcompetence"]) {
                // line 37
                echo "                            <tr>
                                <td>";
                // line 38
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["microcompetence"], "id", [], "any", false, false, false, 38), "html", null, true);
                echo "</td>
                                <td>";
                // line 39
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["microcompetence"], "description", [], "any", false, false, false, 39), "html", null, true);
                echo "</td>
                                <td>
                                    <a href=\"";
                // line 41
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("micro_competence_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["microcompetence"], "id", [], "any", false, false, false, 41)]), "html", null, true);
                echo "\">
                                        <button class=\"btn btn-sm btn-secondary\"><i class=\"fas fa-edit\"></i></button>
                                    </a>
                                </td>
                            </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['microcompetence'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "                    </tbody>
                </table>
            </div>
        ";
        }
        // line 51
        echo "    </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 56
    public function block_otherscripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "otherscripts"));

        // line 57
        echo "    <script>
        // Afficher uniquement les options pour la compétence analyse dans un premier temps
        for(let option of document.getElementsByClassName(\"conception\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"realisation\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"deploiement\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"gestion\")) option.hidden = true;

        // Changer les options si changement de compétence
        \$('#admin_micro_competence_competence').on('change', function() {
            var selectCompetence = this.options[this.selectedIndex].text;
            console.log(selectCompetence);
            var options = document.getElementsByClassName(selectCompetence);
            var elements  = \$(\".deploiement, .analyse, .conception, .realisation, .gestion\");
            for (let option of elements) option.hidden = true;
            for (let option of options) option.hidden = false;
            for (let checkbox of document.getElementsByTagName('input')) checkbox.checked = false;
            document.getElementById('admin_micro_competence_sousCompetence').value = options[0].value;
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "micro_competence/adminMicroCompetence.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 57,  167 => 56,  157 => 51,  151 => 47,  139 => 41,  134 => 39,  130 => 38,  127 => 37,  123 => 36,  113 => 28,  111 => 27,  101 => 20,  96 => 18,  92 => 17,  88 => 16,  84 => 15,  74 => 7,  67 => 6,  54 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Editer des micro compétences{% endblock %}


{% block body %}
<div class=\"tab_container\">
    <h1>Modifier des micro compétences</h1>
    <hr class=\"my-4\">

    <div class=\"row\">
        <div class=\"au-card col-md-5\">
            <h5>Chercher une micro compétence à éditer</h5>
            <div class=\"line\"></div>
            {{ form_start(form) }}
                {{ form_widget(form) }}
                <button class=\"btn btn-style-2\">{{ button_label|default('Rechercher les microcompétences') }}</button>
            {{ form_end(form) }}
            <div class=\"line\"></div>
            <a href=\"{{ path('micro_competence_new')}}\">
                <button class=\"btn btn-sm btn-secondary float-right\">
                    <i class=\"fas fa-plus\"></i>
                    Nouvelle micro compétence
                </button>
            </a>
        </div>
        {% if microcompetences is defined and microcompetences is not empty %}
            <div class=\"au-card col-md-6\">
                <table class=\"table table-hover tableProfil tableStyle\">
                    <thead>
                        <th scope=\"row\">Id</th>
                        <th>Microcompétence</th>
                        <th>Editer</th>
                    </thead>
                    <tbody>
                        {% for microcompetence in microcompetences %}
                            <tr>
                                <td>{{ microcompetence.id }}</td>
                                <td>{{ microcompetence.description }}</td>
                                <td>
                                    <a href=\"{{ path('micro_competence_edit', {id: microcompetence.id})}}\">
                                        <button class=\"btn btn-sm btn-secondary\"><i class=\"fas fa-edit\"></i></button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                    </tbody>
                </table>
            </div>
        {% endif %}
    </div>
</div>

{% endblock %}

{% block otherscripts %}
    <script>
        // Afficher uniquement les options pour la compétence analyse dans un premier temps
        for(let option of document.getElementsByClassName(\"conception\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"realisation\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"deploiement\")) option.hidden = true;
        for(let option of document.getElementsByClassName(\"gestion\")) option.hidden = true;

        // Changer les options si changement de compétence
        \$('#admin_micro_competence_competence').on('change', function() {
            var selectCompetence = this.options[this.selectedIndex].text;
            console.log(selectCompetence);
            var options = document.getElementsByClassName(selectCompetence);
            var elements  = \$(\".deploiement, .analyse, .conception, .realisation, .gestion\");
            for (let option of elements) option.hidden = true;
            for (let option of options) option.hidden = false;
            for (let checkbox of document.getElementsByTagName('input')) checkbox.checked = false;
            document.getElementById('admin_micro_competence_sousCompetence').value = options[0].value;
        });
    </script>
{% endblock %}
", "micro_competence/adminMicroCompetence.html.twig", "/var/www/tx_back/competences_isi/templates/micro_competence/adminMicroCompetence.html.twig");
    }
}
